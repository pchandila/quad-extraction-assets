#!/bin/bash

# Deploys RPM to Artifactory
# First argument ($1) to this script is the RPM file
# Assumes that quad-rpms-dev, quad-rpms-rc, and quad-rpms-prod are valid YUM repos in Artifactory

RPM="$1"
if [ ! -f "$RPM" ]; then
    echo "RPM file not found!"
    exit -1
fi

REPO="quad-rpms-rc"
if [ "$CIRCLE_BRANCH" == 'dev' ] || [ "$CIRCLE_BRANCH" == 'prod' ] ; then
    REPO="quad-rpms-$CIRCLE_BRANCH"
fi

echo -e "Deploying $RPM to $REPO ...\n"

SHA1=$(sha1sum -b $RPM | cut -f1 -d' ')
ARTIFACTORY_URL="https://quadanalytix.artifactoryonline.com/quadanalytix/$REPO/centos/noarch"
PUT_URL="$ARTIFACTORY_URL/$CIRCLE_PROJECT_REPONAME/$(basename $RPM);branch=$CIRCLE_BRANCH;build.number=$CIRCLE_BUILD_NUM"

curl -u deployer:$JFROG_DEPLOYER_PASSWORD -H "X-Checksum-Sha1:$SHA1" -X PUT "$PUT_URL" -T "$RPM"
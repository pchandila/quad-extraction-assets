function filterUrl(url){
    var uniqueAttrs = [];
    
    if(url.indexOf("?") >= 0){
    	url = url.split('?')[0]
    }
    
    if(url.indexOf("/") >= 0){
        var split_array = url.split('/');
        var ua = split_array[split_array.length-1]
        uniqueAttrs.push(ua);
    }
        
    return uniqueAttrs
}
function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    var regex = new RegExp("[a-zA-Z0-9;\\-]+\\-p[0-9]+$");
    if(split.length < 4 || !regex.test(split[3])) {
    	return uniqueAttrs;
    }
    var productCode = split[3].substring(split[3].lastIndexOf("-p") + 1);
    uniqueAttrs.push(split[2]);
    uniqueAttrs.push(productCode);
    return uniqueAttrs;
}
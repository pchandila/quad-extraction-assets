function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length <3) {
        return uniqueAttrs;
    } 
    var attributes = url.split("?")[1].split("&");
    for (i = 0; i < attributes.length; i++) { 
        if((attributes[i].indexOf("id") == 0) || (attributes[i].indexOf("skuId") == 0)) {
            uniqueAttrs.push(attributes[i].split("=")[1])
        }
    }
   return uniqueAttrs;
}
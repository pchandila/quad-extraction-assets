function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 3) {
      return uniqueAttrs;
    }
    uniqueAttrs.push(split[2]);
    for(i = 0; i < split.length; i++) {
        var s = split[i];
        if(s.indexOf("p-") == 0) {
            uniqueAttrs.push(s.substring(2, s.indexOf("?")));
        }
    }
    return uniqueAttrs;
}
function filterUrl(url){
    var uniqueAttrs = [];
    var index = url.indexOf("?");
    if(index >= 0){
        var params = url.split('?')[1].split('&');
        
        for (var i = 0; i < params.length; i++){
            if(params[i].split("=")[0] == 'productId'){
                uniqueAttrs.push(params[i].split('=')[1]);
                break;
            }
        }
    }
    return uniqueAttrs
}
function filterURL(url)
{
    //1.http://www.zappos.com/p/laredo-crocoloco-cognac-croc-print/product/8543471/color/564596
    //2.http://www.zappos.com/product/8294688/color/543174
    //3.http://www.zappos.com/crocs-a-leigh-brushed-metallic-flip-sliver
	var uniqueAttrs = [];
	if(url.indexOf("?") > -1 && url.indexOf("//") > -1)
	{
		url = url.split("?")[0].split("//")[1];
	}
	else if(url.indexOf("//") > -1)
	{
		url = url.split("//")[1];
	}
	if(url.indexOf("/") > -1)
	{
		var res = url.split("/"); 
		if(res.length>1 && res.length<3)
		{
			uniqueAttrs.push(res[1]);
		}
        else if(res.length>1)
        {
            var index_of_product = res.indexOf("product");
            uniqueAttrs.push(res[index_of_product + 1]);
        }
	}
	return uniqueAttrs;
}
function filterUrl(url){
    var uniqueAttrs = [];
    
    if(url.indexOf("?") >= 0)
        url = url.split('?')[0]
    
    if(url.indexOf("/") >= 0){
        var params = url.split('/').pop()
        if(params.indexOf("-sku-") >=0){
            unq_id = params.split('-sku-').pop()
            uniqueAttrs.push(unq_id.replace('.html',''))
        }
    }
    return uniqueAttrs
}
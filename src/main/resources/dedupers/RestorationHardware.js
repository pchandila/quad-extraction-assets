function(url) {
  var uniqueAttrs = [];
    var split = url.split("?");
    if(split.length > 1) {
    	var cleansedURL = split[1].split("&")[0];
    	if(cleansedURL.indexOf("productId") != -1) {
    		var productId = cleansedURL.replace("productId=", "").replace("prod", "");
    		uniqueAttrs.push(productId);
    	}
    }
    return uniqueAttrs;
}
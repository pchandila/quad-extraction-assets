function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 4) {
    	return uniqueAttrs;
    }
    if("product" != split[3].toLowerCase()) {
    	return uniqueAttrs;
    }
    
    var attributes = url.split("?")[1].split("&");
    for(i=0; i < attributes.length; i++) {
    	if(attributes[i].indexOf("ID") == 0) {
    		uniqueAttrs.push(attributes[i].split("=")[1]);
    	}
    }
    return uniqueAttrs;
}
function filterUrl(url){
    var uniqueAttrs = [];
    var index = url.indexOf(".uts");
    if(index >= 0){
        url = url.split('?')[0]
        var params = url.split('.uts')[0];
        var array = params.split('/')
        uniqueAttrs.push(array[array.length -1])
    }
    index = url.indexOf("productId");
    if(index >= 0){
        var params = url.split("productId=")
        if(params.length == 2){
            var product_id = params[1]
            var array = product_id.split('&')
            uniqueAttrs.push(array[0])
        }
    }
    return uniqueAttrs
}
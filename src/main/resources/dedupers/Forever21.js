function filterUrl(url){
    var uniqueAttrs = [];
    
    if(url.indexOf("&") >= 0){
        var url_split_values = url.split('&');
        for (index in url_split_values){
        	if(url_split_values[index].indexOf("ProductID=") >= 0){
        		var product = url_split_values[index].split('ProductID=')[1];
        		uniqueAttrs.push(product);
        	}
        }
    }    
    return uniqueAttrs;
}
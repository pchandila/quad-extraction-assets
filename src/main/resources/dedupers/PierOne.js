function filterURL(url)
{
    //Eg : http://www.pier1.com/Turquesa-Tile-Duvet-Cover-Sham/PS48720,default,pd.html?cgid=patterned-bedding#nav=top&start=1
    //Eg : http://www.pier1.com/Reed-Diffuser---Honeysuckle/2875678,default,pd.html?cgid=reed-diffusers#nav=top&start=1
	var uniqueAttrs = [];
	url = url.split("?")[0];
	if(url.indexOf("/") > -1)
	{
		url = url.split("/");
		for(i=0;i<url.length;i++)
		{
			if(url[i].indexOf('default')>-1){url = url[i].split(',')[0]; break;}
		}
		uniqueAttrs.push(url);
	}
	return uniqueAttrs;
}
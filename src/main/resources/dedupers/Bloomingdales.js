function(url) {
    var uniqueAttrs = [];
    var split = url.split("&");
    if(split.length > 0) {
        var split1 = split[0].split("?");
        if(split1.length > 1) {
            var split2 = split1[split1.length - 1].split("=");
            if(split2.length > 1) {
                uniqueAttrs.push(split2[1]);
                return uniqueAttrs;
            }
        }
    } 
    return uniqueAttrs;
}
function(url) {
    var uniqueAttrs = [];
    var split1 = url.split("/");
    var split = split1.filter(function(s){ return s!="";});
    if(split.length < 3) {
    	return uniqueAttrs;
    }
    var regex = new RegExp("^[a-z]\\d{5,6}$");
    for (i = 0; i < split.length; i++) {
    	if(regex.test(split[i])) {
    		uniqueAttrs.push(split[i]);
    	}
    }
    return uniqueAttrs;
}
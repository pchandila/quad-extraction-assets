function(url) {
    var uniqueAttrs = [];
    var cleanedURL = url.split("?")[0].split("/").filter(function(s){ return s!="";});
    if("ip" == cleanedURL[2]) {
        if(!isNaN(cleanedURL[4])) {
            uniqueAttrs.push(cleanedURL[4]);
        } else if(!isNaN(cleanedURL[3])) {
            uniqueAttrs.push(cleanedURL[3]);
        }
    }
    return uniqueAttrs;
}
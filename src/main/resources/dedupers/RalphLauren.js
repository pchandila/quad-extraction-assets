function filterUrl(url){
    var uniqueAttrs = [];
    var index = url.indexOf("?");
    if(index >= 0){
        var params = url.split('?')[1].split('&');
        
        for (var i in params){
            curr_param = params[i].split("=")[0]
            if(curr_param == 'productId' || curr_param == "reference_number"){
                uniqueAttrs.push(params[i].split('=')[1]);
                break;
            }
        }
    }
    return uniqueAttrs
}
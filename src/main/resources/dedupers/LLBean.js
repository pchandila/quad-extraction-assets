function filterURL(url) {
    //Eg : http://www.llbean.com/llb/shop/68897?feat=513283-GN1&page=easy-stretch-pants-denim
    //Eg : http://www.llbean.com/llb/shop/68419?feat=593-GN1&page=trail-model-rain-pants&attrValue_0=Black&productId=1120925
    var uniqueAttrs = [];
    if(url.indexOf("?")>0)
    {
    	var queryString = url.split("?")[1];
    	var qsVariables = queryString.split("&");
    	for (i = 0; i < qsVariables.length; i++) { 
    		if((qsVariables[i].indexOf("page") == 0)) {
    			uniqueAttrs.push(qsVariables[i].split("=")[1]);
    			break
    		}
    	}
    }
    return uniqueAttrs;
}
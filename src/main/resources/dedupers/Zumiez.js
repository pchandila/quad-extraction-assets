function filterURL(url) {
    //Eg : http://www.zumiez.com/adidas-sl-loop-runner-black-and-grey-shoes.html
    //Eg : http://www.zumiez.com/neff-daily-teal-black-10k-2014-softshell-snowboard-jacket.html
    var uniqueAttrs = [];
    var url = url.split("?")[0];
    var split1 = url.split("/");
    if(split1.length>3)
    {
        var split2 = split1.filter(function(s){ return s!="";});
        url = split2[split2.length-1].split(".")[0];
        uniqueAttrs.push(url);
    }
    return uniqueAttrs;
}
function filterUrl(url){
    var uniqueAttrs = [];
    
    if(url.indexOf("?") >= 0)
        url = url.split('?')[0]
    
    if(url.indexOf(".com/") >= 0){
        var unique_id = url.split('.com/').pop()
        uniqueAttrs.push(unique_id.replace('/',''))
    }
        
    return uniqueAttrs
}
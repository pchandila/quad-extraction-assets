function filterURL(url){
	var garbage_elem = ["previousSort","vDropDown","viewItems","redirectType","lastRating","sLevel","sortOption"];
    var required_elem = [];
	if(url.indexOf("?") > -1)
	{
		var mainUrl = url.split("?");
		var res =  mainUrl[1].split("&");
		for (var i in res) {
        	if(garbage_elem.indexOf(res[i].split("=")[0]) == -1)
        	{
        		required_elem[required_elem.length] = res[i];
                
        	}
    	}
    	var finalUrl = mainUrl[0] + "?" + required_elem.join("&");
		return finalUrl;
	}
	else
	{
		return url;
	}
}
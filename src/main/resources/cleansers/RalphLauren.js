function clean(url) {
    var required_params = []
	var toReturn = url;
	var params_to_retain = ['categoryId', 'ab', 'cp'];
	var index = url.indexOf("?");
	if(index >= 0){
	    var url_n_params = url.split('?');
	    var base_url = url_n_params[0];
	    var params = url_n_params[1].split('&');
	    for (var i = 0; i < params.length; i++) {
	        if(params_to_retain.indexOf(params[i].split("=")[0]) != -1)
        	    required_params[required_params.length] = params[i];    
	    }
	    
	    var finalUrl = base_url + "?" + required_params.join("&");
		return finalUrl;
	}
	return toReturn;
}
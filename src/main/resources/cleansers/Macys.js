function(url) {
	var splits = url.split("&");
	if(splits.length == 0) {
		return null;
	}
	return splits[0];
}
function filterURL(url){
    //EG: http://www.pier1.com/furniture/bedroom-furniture,default,sc.html?sz=60&start=0#nav=left
    //above case we shall remove start=0#nav=left but since contains # in it so ll not be removed
    //Eg: http://www.pier1.com/furniture/living-room/accent-tables/coffee-tables,default,sc.html?icid=cat_indoor_seating-subcat_accent_tables-subcat_tile_coffee_tables&nav=tile
    //in above case nav part is removed
    //Eg: http://www.pier1.com/furniture/bedroom-furniture/beds-daybeds,default,sc.html#nav=top
    //nothing removed in such case as no '?' in url
    
	var garbage_elem = ["start","nav"];
    var required_elem = [];
	if(url.indexOf("?") > -1)
	{
		var mainUrl = url.split("?");
		var res =  mainUrl[1].split("&");
		for (var i in res) {
        	if(garbage_elem.indexOf(res[i].split("=")[0]) == -1)//garbage elem doesnt contain this element
        	{
        		required_elem[required_elem.length] = res[i];
        	}
            else if(res[i].indexOf("#") > -1)//garbage elem has this variable but variable also has # in it
            {
                required_elem[required_elem.length] = res[i];//treat this as required
            }
    	}
        if(required_elem.length>0)
        {
    		var finalUrl = mainUrl[0] + "?" + required_elem.join("&");
        }
        else
        {
            var finalUrl = mainUrl[0];
        }
		return finalUrl;
	}
	else
	{
		return url;
	}
}
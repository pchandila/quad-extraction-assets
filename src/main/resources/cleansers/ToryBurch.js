function clean(url) {
    var required_params = []
	var toReturn = url;
	
	var index = toReturn.indexOf("?");
	if(index >= 0){
	    var url_n_params = toReturn.split('?');
	    toReturn = url_n_params[0];
	}

	index = toReturn.indexOf("#");
	if(index >= 0){
	    url_n_params = toReturn.split('#');
	    toReturn = url_n_params[0];
	}
	return toReturn;
}
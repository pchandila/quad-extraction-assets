function filterURL(url){
	var garbage_elem = ["pageSize","vDropDown","beginIndex","storeId","langId","orderBy","resultCatEntryType","showResultsPage","pageView", 
						"langId","urlRequestType","searchType","searchTermScope","urlLangId","pageSize","minPrice","manufacturer","maxPrice",
						"searchTerm","beginIndex","filterTerm","metaData"];
    var required_elem = [];
	if(url.indexOf("?") > -1)
	{
		var mainUrl = url.split("?");
		var res =  mainUrl[1].split("&");
		for (var i in res) {
        	if(garbage_elem.indexOf(res[i].split("=")[0]) == -1)
        	{
        		required_elem[required_elem.length] = res[i];
                
        	}
    	}
    	var finalUrl = mainUrl[0] + "?" + required_elem.join("&");
		return finalUrl;
	}
	else
	{
		return url;
	}
}
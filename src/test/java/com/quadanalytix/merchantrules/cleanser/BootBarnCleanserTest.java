package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BootBarnCleanserTest extends CleanserTest{
	@Test
    public void testBootBarnCleanser() throws IOException {
        String cleanedURL = getCleanedURL("BootBarn.js", "http://www.bootbarn.com/Work-Shoes/Mens_WorkShoes,default,sc.html?prefn1=gender&prefv1=Men&prefn2=width&prefv2=EW");
        assertEquals("http://www.bootbarn.com/Work-Shoes/Mens_WorkShoes,default,sc.html", cleanedURL);
        
        cleanedURL = getCleanedURL("BootBarn.js", "http://www.bootbarn.com/Fashion/Fashion_Boots,default,sc.html?prefn1=silhouette&prefv1=2");
        assertEquals("http://www.bootbarn.com/Fashion/Fashion_Boots,default,sc.html", cleanedURL);
        
        cleanedURL = getCleanedURL("BootBarn.js", "http://www.bootbarn.com/Western-Work/Western%20Work,default,sc.html?prefn1=heelType&prefv1=350&sz=60");
        assertEquals("http://www.bootbarn.com/Western-Work/Western%20Work,default,sc.html", cleanedURL);
        
        cleanedURL = getCleanedURL("BootBarn.js", "http://www.bootbarn.com/Western-Work/Western%20Work,default,sc.html");
        assertEquals("http://www.bootbarn.com/Western-Work/Western%20Work,default,sc.html", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class PacSunCleanserTest extends CleanserTest {
    @Test
    public void testPacSunCleanser() throws IOException {
        String cleanedURL = getCleanedURL("PacSun.js", "http://www.pacsun.com/womens/skirts/?ICID=0015953");
        assertEquals("http://www.pacsun.com/womens/skirts/", cleanedURL); 
        cleanedURL = getCleanedURL("PacSun.js", "http://www.pacsun.com/mens/sunglasses/?ICID=0013131&start=24");
        assertEquals("http://www.pacsun.com/mens/sunglasses/", cleanedURL); 
        cleanedURL = getCleanedURL("PacSun.js", "http://www.pacsun.com/mens/sunglasses/");
        assertEquals("http://www.pacsun.com/mens/sunglasses/", cleanedURL); 
        
    }
}

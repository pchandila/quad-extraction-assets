package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class ToysRUSCleanserTest extends CleanserTest {
    @Test
    public void testToysRUSCleanser() throws IOException {
        String cleanedURL = getCleanedURL("ToysRUS.js", "http://www.toysrus.com/family/index.jsp?categoryId=10811246");
        assertEquals("http://www.toysrus.com/family/index.jsp?categoryId=10811246", cleanedURL);
        
        cleanedURL = getCleanedURL("ToysRUS.js", "http://www.toysrus.com/family/index.jsp?categoryId=3176425&cp=2255963.4174554");
        assertEquals("http://www.toysrus.com/family/index.jsp?categoryId=3176425", cleanedURL);
        
        cleanedURL = getCleanedURL("ToysRUS.js", "http://www.toysrus.com/family/index.jsp?categoryId=3176425&cp=2255963.4174554");
        assertNotEquals("http://www.toysrus.com/family/index.jsp?categoryId=3176425&cp=2255963.4174554", cleanedURL);
        
        cleanedURL = getCleanedURL("ToysRUS.js", "http://www.toysrus.com/search/index.jsp?categoryId=50240936&fg=Brand&ff=PAD&fv=Brand+Name+Secondary%2FDisney+Princess&fd=Disney+Princess&");
        assertEquals("http://www.toysrus.com/search/index.jsp?categoryId=50240936&fg=Brand&ff=PAD&fv=Brand+Name+Secondary%2FDisney+Princess&fd=Disney+Princess&", cleanedURL);
        
        cleanedURL = getCleanedURL("ToysRUS.js", "http://www.toysrus.com/search/index.jsp?categoryId=50240936&fg=Brand&ff=PAD&fv=Brand+Name+Secondary%2FDisney+Princess&fd=Disney+Princess&");
        assertNotEquals("http://www.toysrus.com/search/index.jsp?categoryId=50240936", cleanedURL);
    }
}

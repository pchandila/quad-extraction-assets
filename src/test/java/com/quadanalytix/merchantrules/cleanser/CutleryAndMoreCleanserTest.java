package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CutleryAndMoreCleanserTest extends CleanserTest {
    @Test
    public void testCutleryAndMoreCleanser() throws IOException {
        String cleanedURL = getCleanedURL("CutleryandMore.js", "http://www.cutleryandmore.com/kitchen-and-food/knife-sharpeners/1");
        assertEquals("http://www.cutleryandmore.com/kitchen-and-food/knife-sharpeners/1", cleanedURL); 
    }
}

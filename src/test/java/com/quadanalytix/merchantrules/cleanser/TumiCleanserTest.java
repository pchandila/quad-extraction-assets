package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class TumiCleanserTest extends CleanserTest {

	@Test
    public void testTumiCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Tumi.js", "http://www.tumi.com/c/outerwear");
        assertEquals("http://www.tumi.com/c/outerwear", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("Tumi.js", "http://www.tumi.com/c/travel#esp_cf=product_type&esp_filter_product_type=Carry-On%20Luggage");
        assertEquals("http://www.tumi.com/c/travel", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("Tumi.js", "http://www.tumi.com/c/outerwear#esp_sort=name&esp_order=desc");
        assertEquals("http://www.tumi.com/c/outerwear", cleanedURL3);
        
        String cleanedURL4 = getCleanedURL("Tumi.js", "http://www.tumi.com/c/accessories-belts#esp_sort=Launch_Date&esp_order=desc");
        assertNotEquals("http://www.tumi.com/c/#", cleanedURL4);       
	}
}
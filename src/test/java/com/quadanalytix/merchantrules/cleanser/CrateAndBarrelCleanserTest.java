package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CrateAndBarrelCleanserTest extends CleanserTest {
    @Test
    public void testCrateAndBarrelCleanser() throws IOException {
        String cleanedURL = getCleanedURL("CrateandBarrel.js", "http://www.crateandbarrel.com/kitchen-and-food/knife-sharpeners/1");
        assertEquals("http://www.crateandbarrel.com/kitchen-and-food/knife-sharpeners/", cleanedURL);
        
        cleanedURL = getCleanedURL("CrateandBarrel.js", "http://www.crateandbarrel.com/kitchen-and-food/knife-sharpeners/");
        assertEquals("http://www.crateandbarrel.com/kitchen-and-food/knife-sharpeners/", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class WalmartCleanserTest extends CleanserTest {   
    @Test
    public void testWalmartCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Walmart.js", "http://www.walmart.com/browse/video-games/accessories/2636_1040581/?browsein=true&_refineresult=true&povid=cat1086506-env414475-moduleA112113-lLinkLHNVideoGames&fromPageCatId=1086506&catNavId=1086506");
        assertEquals("http://www.walmart.com/browse/video-games/accessories/2636_1040581/", cleanedURL);
        
        cleanedURL = getCleanedURL("Walmart.js", "http://www.walmart.com/cp/College-Living/1093705?povid=cat1070145-env172199-module061414-lLinkGNAV_COLLEGE");
        assertEquals("http://www.walmart.com/cp/College-Living/1093705", cleanedURL);
        
        cleanedURL = getCleanedURL("Walmart.js", "http://www.walmart.com/browse/home/coffee-espresso-makers/4044_90548_90546_1115306/");
        assertEquals("http://www.walmart.com/browse/home/coffee-espresso-makers/4044_90548_90546_1115306/", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class RevolveclothingCleanserTest extends CleanserTest{
	@Test
    public void testRevolveclothingCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Revolveclothing.js", "http://www.revolve.com/denim-flared-wide-leg/br/0aeac2/?color%5B%5D=dark+rinse&color%5B%5D=neutral");
        assertEquals("http://www.revolve.com/denim-flared-wide-leg/br/0aeac2/", cleanedURL);
        
        cleanedURL = getCleanedURL("Revolveclothing.js", "http://www.revolve.com/bags-crossbody-bags/br/f8c179/");
        assertEquals("http://www.revolve.com/bags-crossbody-bags/br/f8c179/", cleanedURL);
        
        cleanedURL = getCleanedURL("Revolveclothing.js", "http://www.revolve.com/bags-travel-bags/br/97907e/?navsrc=left&designer%5B%5D=Patagonia");
        assertEquals("http://www.revolve.com/bags-travel-bags/br/97907e/", cleanedURL);
  
	}
}

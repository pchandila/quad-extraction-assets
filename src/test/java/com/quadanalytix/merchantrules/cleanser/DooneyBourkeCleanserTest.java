package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class DooneyBourkeCleanserTest extends CleanserTest {

	@Test
    public void testDooneyBourkeCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("DooneyBourke.js", "http://www.dooney.com/dooney-bourke/bags/");
        assertEquals("http://www.dooney.com/dooney-bourke/bags/", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("DooneyBourke.js", "http://www.dooney.com/the-collegiate-shop/auburn-university/?pmax=99&prefn1=collection&prefn2=material&prefv3=NA&prefv4=Travel&pmin=0&prefv1=Collegiate&prefv2=Coated%20Cotton&prefn3=refinementColor&prefn4=style");
        assertEquals("http://www.dooney.com/the-collegiate-shop/auburn-university/", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("DooneyBourke.js", "http://www.dooney.com/mlb/los-angeles-dodgers/?pmax=300&prefn1=collection&prefn2=material&prefv3=NA&prefv4=Crossbody&pmin=200&prefv1=MLB&prefv2=Leather&prefn3=refinementColor&prefn4=style");
        assertEquals("http://www.dooney.com/mlb/los-angeles-dodgers/", cleanedURL3);
        
        String cleanedURL4 = getCleanedURL("DooneyBourke.js", "http://www.dooney.com/dooney-bourke/accessories/?pmax=99&prefn1=collection&prefn2=material&prefv3=BLUE&pmin=0&prefv1=Cabriolet&prefv2=Fabric&prefn3=refinementColor");
        assertNotEquals("http://www.dooney.com/dooney-bourke/accessories/?", cleanedURL4);
        
        
        
        
        
       
  
	}
}
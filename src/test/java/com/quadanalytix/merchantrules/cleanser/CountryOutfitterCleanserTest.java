package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CountryOutfitterCleanserTest extends CleanserTest{
	@Test
    public void testCountryOutfitterCleanser() throws IOException {
        String cleanedURL = getCleanedURL("CountryOutfitter.js", "http://www.countryoutfitter.com/booties/womens/#prefn1=refinementColor&prefn2=size&prefv1=Brown&prefv2=9.5");
        assertEquals("http://www.countryoutfitter.com/booties/womens/", cleanedURL);
        
        cleanedURL = getCleanedURL("CountryOutfitter.js", "http://www.countryoutfitter.com/kids/clothing/bottoms/");
        assertEquals("http://www.countryoutfitter.com/kids/clothing/bottoms/", cleanedURL);
        
        cleanedURL = getCleanedURL("CountryOutfitter.js", "http://www.countryoutfitter.com/womens/skirts/#prefn1=brand&prefv1=Angie");
        assertEquals("http://www.countryoutfitter.com/womens/skirts/", cleanedURL);
        
        cleanedURL = getCleanedURL("CountryOutfitter.js", "http://www.countryoutfitter.com/jeans/womens/#prefn1=cutfit&prefn2=refinementColor&prefv1=Low%20Rise&prefv2=Blue");
        assertEquals("http://www.countryoutfitter.com/jeans/womens/", cleanedURL);
    }
}

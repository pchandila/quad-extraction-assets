package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class PaulasChoiceCleanserTest extends CleanserTest {
    @Test
    public void testPaulasChoiceCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("PaulasChoice.js", "http://www.paulaschoice.com/skin-care-concerns/blackheads/?skintypeorconcern1=Blackheads");
        assertEquals("http://www.paulaschoice.com/skin-care-concerns/blackheads/", cleanedURL);
        
        String cleanedURL3 = getCleanedURL("PaulasChoice.js", "http://www.paulaschoice.com/skin-types/combination/?skintypeorconcern1=Combination");
        assertEquals("http://www.paulaschoice.com/skin-types/combination/", cleanedURL3);
        
        String cleanedURL1 = getCleanedURL("PaulasChoice.js", "http://www.paulaschoice.com/skin-types/combination/");
        assertEquals("http://www.paulaschoice.com/skin-types/combination/", cleanedURL1);
    	
       
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class DicksSportingGoodsCleanserTest extends CleanserTest {
    @Test
    public void testDicksSportingGoodsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("DicksSportingGoods.js", "http://www.dickssportinggoods.com/family/index.jsp?categoryId=4414160&bc=CatGroup_GolfClubs_R2_C2_Wedges");
        assertEquals("http://www.dickssportinggoods.com/family/index.jsp?categoryId=4414160", cleanedURL); 
        cleanedURL = getCleanedURL("DicksSportingGoods.js", "http://www.dickssportinggoods.com/family/index.jsp?ab=TopNav_Golf_GolfBagsCarts_TravelCovers&categoryId=4414224&cp=4413989.4414127");
        assertEquals("http://www.dickssportinggoods.com/family/index.jsp?categoryId=4414224&cp=4413989.4414127", cleanedURL); 
        
    }
}

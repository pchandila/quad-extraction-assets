package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BackcountryCleanserTest extends CleanserTest {

	@Test
    public void testBackcountryCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Backcountry.js", "http://www.backcountry.com/womens-rain-jackets?p=attr_length%3AHip&nf=1");
        assertEquals("http://www.backcountry.com/womens-rain-jackets", cleanedURL);
        
        cleanedURL = getCleanedURL("Backcountry.js", "http://www.backcountry.com/womens-casual-boots-shoes");
        assertEquals("http://www.backcountry.com/womens-casual-boots-shoes", cleanedURL);
        
        cleanedURL = getCleanedURL("Backcountry.js", "http://www.backcountry.com/womens-clogs?p=brand%3AColumbia|color%3Ared&nf=1");
        assertEquals("http://www.backcountry.com/womens-clogs", cleanedURL);
  
	}
}
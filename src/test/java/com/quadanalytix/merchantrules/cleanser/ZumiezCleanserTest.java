package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class ZumiezCleanserTest extends CleanserTest {
    @Test
    public void testZumiezCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Zumiez.js", "http://www.zumiez.com/brands/vans/vans-shoes.html?color=black&d=1000107%2C4294966298&gender=womens&order=name");
        assertEquals("http://www.zumiez.com/brands/vans/vans-shoes.html?color=black&d=1000107%2C4294966298&gender=womens", cleanedURL);
        
        cleanedURL = getCleanedURL("Zumiez.js", "http://www.zumiez.com/brands/vans/vans-shoes.html?d=1000107&gender=womens&order=name");
        assertEquals("http://www.zumiez.com/brands/vans/vans-shoes.html?d=1000107&gender=womens", cleanedURL);
        
        cleanedURL = getCleanedURL("Zumiez.js", "http://www.zumiez.com/accessories/belts.html");
        assertEquals("http://www.zumiez.com/accessories/belts.html", cleanedURL);
    }
}
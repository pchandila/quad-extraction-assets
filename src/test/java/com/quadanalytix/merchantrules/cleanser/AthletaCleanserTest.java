package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class AthletaCleanserTest extends CleanserTest {
    @Test
    public void testAthletaCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Athleta.js", "http://athleta.gap.com/browse/category.do?cid=46693");
        assertEquals("http://athleta.gap.com/browse/category.do?cid=46693", cleanedURL);
        
        cleanedURL = getCleanedURL("Athleta.js", "http://athleta.gap.com/browse/category.do?cid=1039151&mlink=46793,10201095,Fit6_City_9_8&clink=10201095#style=1039157");
        assertEquals("http://athleta.gap.com/browse/category.do?cid=1039151#style=1039157", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class EddieBauerCleanserTest extends CleanserTest {
    @Test
    public void testEddieBauerCleanser() throws IOException {
    
    	String cleanedURL2 = getCleanedURL("EddieBauer.js", "http://www.eddiebauer.com/browse/_/N-1z13x1x?previouspage=SCAt&cm_re=mens-_-1_fl_p-_-GrillMaster_150529");
        assertEquals("http://www.eddiebauer.com/browse/_/N-1z13x1x", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("EddieBauer.js", "http://www.eddiebauer.com/browse/_/N-1z13x1x?previouspage=SCAt&cm_re=mens-_-1_fl_p-_-GrillMaster_150529");
        assertNotEquals("http://www.eddiebauer.com/browse/_/N-1z13x1x?previouspage=SCAt&cm_re=mens-_-1_fl_p-_-GrillMaster_150529", cleanedURL2);
    
    	String cleanedURL3 = getCleanedURL("EddieBauer.js", "http://www.eddiebauer.com/search/?Dy=1&Nty=1&N=0&previousPage=SRC&Ntt=full+shoes&x=0&y=0&_requestid=463591");
        assertEquals("http://www.eddiebauer.com/search/?Ntt=full+shoes", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("EddieBauer.js", "http://www.eddiebauer.com/search/?Dy=1&Nty=1&N=0&previousPage=SRC&Ntt=full+shoes&x=0&y=0&_requestid=463591");
        assertNotEquals("http://www.eddiebauer.com/search/?Dy=1&Nty=1&N=0&previousPage=SRC&Ntt=full+shoes&x=0&y=0&_requestid=463591", cleanedURL3);
    
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class DillardsCleanserTest extends CleanserTest {
    @Test
    public void testDillardsCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("Dillards.js", "http://www.dillards.com/c/women-dresses#pageSize=96&beginIndex=0&orderBy=1");
        assertEquals("http://www.dillards.com/c/women-dresses", cleanedURL);
        String cleanedURL1 = getCleanedURL("Dillards.js", "http://www.dillards.com/search-term/Lace?categoryId=894");
        assertEquals("http://www.dillards.com/search-term/Lace?categoryId=894", cleanedURL1);
        String cleanedURL2 = getCleanedURL("Dillards.js", "http://www.dillards.com/search-term/0135+OR+0134+OR+0136+OR+0137+0160?categoryId=604007&forceFlatResults=forceFlatResults#pageSize=100&beginIndex=0&orderBy=1");
        assertEquals("http://www.dillards.com/search-term/0135+OR+0134+OR+0136+OR+0137+0160?categoryId=604007&forceFlatResults=forceFlatResults", cleanedURL2);
        
    }
}

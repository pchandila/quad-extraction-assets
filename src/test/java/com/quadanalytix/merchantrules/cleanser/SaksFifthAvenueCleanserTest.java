package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class SaksFifthAvenueCleanserTest extends CleanserTest {

	@Test
    public void testJetCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/Miu-Miu/Women-s-Apparel/shop/_/N-1z12v8wZ52flog?category=Women-s-Apparel&email=paolrezk%40yahoo.fr&rremail=paolrezk%40yahoo.fr&site_refer=EML14662PROM1123&website=true&brandLanding=true");
        assertEquals("http://www.saksfifthavenue.com/Miu-Miu/Women-s-Apparel/shop/_/N-1z12v8wZ52flog", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/Missoni-Home/Women-s-Apparel/shop/_/N-1z12vggZ52flog?category=Women-s-Apparel&email=paolrezk%40yahoo.fr&rremail=paolrezk%40yahoo.fr&site_refer=EML14662PROM1123&website=true&brandLanding=true");
        assertEquals("http://www.saksfifthavenue.com/Missoni-Home/Women-s-Apparel/shop/_/N-1z12vggZ52flog", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/Mimi-Holliday/Women-s-Apparel/shop/_/N-1z12qdvZ52flog?category=Women-s-Apparel&email=paolrezk%40yahoo.fr&rremail=paolrezk%40yahoo.fr&site_refer=EML14662PROM1123&website=true&brandLanding=true");
        assertNotEquals("http://www.saksfifthavenue.com/Mimi-Holliday/Women-s-Apparel/shop/_/N-1z12qdvZ52flog?", cleanedURL3);       
        
	}
}
package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class UltaCleanserTest extends CleanserTest {
    @Test
    public void testultaCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("Ulta.js", "http://www.ulta.com/makeup-face-foundation?N=1z13uu6Z1z140yiZ26y5&Ns=product.price|1");
        assertEquals("http://www.ulta.com/makeup-face-foundation?N=1z13uu6Z1z140yiZ26y5&Ns=product.price|1", cleanedURL);
        String cleanedURL1 = getCleanedURL("Ulta.js", "http://www.ulta.com/bath-body-body-moisturizers-body-oils?N=26v6");
        assertEquals("http://www.ulta.com/bath-body-body-moisturizers-body-oils?N=26v6", cleanedURL1);
    }
}

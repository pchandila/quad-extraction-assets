package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class FootLockerCleanserTest extends CleanserTest {
    @Test
    public void testFootLockerCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("FootLocker.js", "http://www.footlocker.com/Mens/T-Shirts/_-_/N-24Zif");
        assertEquals("http://www.footlocker.com/Mens/T-Shirts/_-_/N-24Zif", cleanedURL);
        String cleanedURL1 = getCleanedURL("FootLocker.js", "http://www.footlocker.com/adidas/_-_/N-zc?Nr=AND%28P%5FRecordType%3AProduct%29");
        assertEquals("http://www.footlocker.com/adidas/_-_/N-zc", cleanedURL1);
        String cleanedURL2 = getCleanedURL("FootLocker.js", "http://www.footlocker.com/Timberland/_-_/N-zt");
        assertEquals("http://www.footlocker.com/Timberland/_-_/N-zt", cleanedURL2);
        
    }
}

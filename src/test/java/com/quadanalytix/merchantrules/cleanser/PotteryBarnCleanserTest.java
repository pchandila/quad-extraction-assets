package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class PotteryBarnCleanserTest extends CleanserTest {
    @Test
    public void testPotteryBarnCleanser() throws IOException {
        String cleanedURL = getCleanedURL("PotteryBarn.js", "http://www.potterybarn.com/shop/rugs-windows/outdoor-rugs-door-mats/?cm_type=gnav");
        assertEquals("http://www.potterybarn.com/shop/rugs-windows/outdoor-rugs-door-mats", cleanedURL);
        
        cleanedURL = getCleanedURL("PotteryBarn.js", "http://www.potterybarn.com/shop/dinnerware-entertaining/kitchen-storage-organization/?cm_type=gnav");
        assertEquals("http://www.potterybarn.com/shop/dinnerware-entertaining/kitchen-storage-organization", cleanedURL);
    }
}

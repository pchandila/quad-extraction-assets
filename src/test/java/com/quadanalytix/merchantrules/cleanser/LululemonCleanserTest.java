package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class LululemonCleanserTest extends CleanserTest {
    @Test
    public void testLululemonCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/7-8-pants-run?categoryId=7-8-pants-run&sort=topSellers&filterType1=size&filterValue1=2");
        assertEquals("http://shop.lululemon.com/products/category/7-8-pants-run?categoryId=7-8-pants-run&filterType1=size&filterValue1=2", cleanedURL); 
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/yoga-clothes?lnid=ln;women;features;yoga");
        assertEquals("http://shop.lululemon.com/products/category/yoga-clothes?lnid=ln;women;features;yoga", cleanedURL);
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/products/category/mens-tops-to-and-from");
        assertEquals("http://shop.lululemon.com/products/category/mens-tops-to-and-from", cleanedURL);
        
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/c/men-tops/_/N-1z140uoZ87xZ1z13ysfZ1z13yrr");
        assertEquals("http://shop.lululemon.com/c/men-tops/_/N-1z140uoZ87xZ1z13ysfZ1z13yrr", cleanedURL);
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/c/jackets-and-hoodies-jackets/_/N-8a1");
        assertEquals("http://shop.lululemon.com/c/jackets-and-hoodies-jackets/_/N-8a1", cleanedURL);
        cleanedURL = getCleanedURL("Lululemon.js", "http://shop.lululemon.com/c/mens-jackets-and-hoodies-jackets/_/N-8at");
        assertEquals("http://shop.lululemon.com/c/mens-jackets-and-hoodies-jackets/_/N-8at", cleanedURL);

        
    }
}

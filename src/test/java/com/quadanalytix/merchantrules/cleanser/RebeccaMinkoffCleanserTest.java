package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class RebeccaMinkoffCleanserTest extends CleanserTest {
    @Test
    public void testRebeccaMinkoffCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/sale?cat=65&dir=asc&limit=90&order=position");
        assertEquals("http://www.rebeccaminkoff.com/sale?cat=65", cleanedURL);
        String cleanedURL1 = getCleanedURL("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/clothing?dir=asc&limit=all&order=name");
        assertEquals("http://www.rebeccaminkoff.com/clothing?dir=asc", cleanedURL1);
        String cleanedURL2 = getCleanedURL("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/clothing?dir=asc&order=position");
        assertEquals("http://www.rebeccaminkoff.com/clothing?dir=asc", cleanedURL2);
        String cleanedURL3 = getCleanedURL("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/clothing");
        assertEquals("http://www.rebeccaminkoff.com/clothing", cleanedURL3);
        
    }
}

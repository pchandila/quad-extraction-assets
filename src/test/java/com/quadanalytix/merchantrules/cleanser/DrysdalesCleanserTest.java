package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class DrysdalesCleanserTest extends CleanserTest {
    @Test
    public void testDSWCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("Drysdales.js", "http://www.drysdales.com/jeans/mens-jeans.html?brand=1763&dir=asc&nro_custom_type=3733&order=waist");
        assertEquals("http://www.drysdales.com/jeans/mens-jeans.html?brand=1763&dir=asc&nro_custom_type=3733&order=waist", cleanedURL);
        String cleanedURL1 = getCleanedURL("Drysdales.js", "http://www.drysdales.com/jeans/mens-jeans.html");
        assertEquals("http://www.drysdales.com/jeans/mens-jeans.html", cleanedURL1);
        
    }
}

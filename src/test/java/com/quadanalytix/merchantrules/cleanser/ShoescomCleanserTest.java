package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class ShoescomCleanserTest extends CleanserTest {

	@Test
    public void testShoescomCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Shoescom.js", "http://www.shoes.com/womens-comfort-foot-health-list2_ct1,oc407,sy1037/1/60");
        assertEquals("http://www.shoes.com/womens-comfort-foot-health-list2_ct1,oc407,sy1037/1/60", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("Shoescom.js", "http://www.shoes.com/womens-clothing-dresses-and-skirts-list2_ct158/1/60");
        assertEquals("http://www.shoes.com/womens-clothing-dresses-and-skirts-list2_ct158/1/60", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("Shoescom.js", "http://www.shoes.com/girls-ariat-list2_br221/1/60");
        assertNotEquals("http://www.shoes.com/girls-ariat-list2_br221/1/60?", cleanedURL3);       
	}
}
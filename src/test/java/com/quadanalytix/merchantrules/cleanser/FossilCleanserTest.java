package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class FossilCleanserTest extends CleanserTest{
	@Test
    public void testFossilCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Fossil.js", "https://www.fossil.com/us/en/women/handbags/travel-and-backpacks.html");
        assertEquals("https://www.fossil.com/us/en/women/handbags/travel-and-backpacks.html", cleanedURL);
        
        cleanedURL = getCleanedURL("Fossil.js", "https://www.fossil.com/us/en/wearable-technology/connected-accessories/straps.html?filter=xf_cas_f2_ntk_cs:Multi&filter=xf_cas_f2_ntk_cs:Red");
        assertEquals("https://www.fossil.com/us/en/wearable-technology/connected-accessories/straps.html", cleanedURL);
        
        cleanedURL = getCleanedURL("Fossil.js", "https://www.fossil.com/us/en/bags/womens-handbag-collections/piper-collection.html?filter=xf_cas_f2_ntk_cs:Blue");
        assertEquals("https://www.fossil.com/us/en/bags/womens-handbag-collections/piper-collection.html", cleanedURL);
  
	}

}

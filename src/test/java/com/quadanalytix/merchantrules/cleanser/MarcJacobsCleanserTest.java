package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class MarcJacobsCleanserTest extends CleanserTest{
	@Test
    public void testMarcJacobsCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("MarcJacobs.js", "http://www.marcjacobs.com/women/bags-wallets/shoulder-bags/#pmax=1001&pmin=501");
        assertEquals("http://www.marcjacobs.com/women/bags-wallets/shoulder-bags/", cleanedURL);
        
        cleanedURL = getCleanedURL("MarcJacobs.js", "http://www.marcjacobs.com/men/sunglasses/?prefn1=brandCode&prefv1=MBM");
        assertEquals("http://www.marcjacobs.com/men/sunglasses/", cleanedURL);
        
        cleanedURL = getCleanedURL("MarcJacobs.js", "http://www.marcjacobs.com/men/ready-to-wear/");
        assertEquals("http://www.marcjacobs.com/men/ready-to-wear/", cleanedURL);
        
	}
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CavendersCleanserTest extends CleanserTest{
	@Test
    public void testCavendersCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Cavenders.js", "https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes/work-boots?offset=48");
        assertEquals("https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes/work-boots", cleanedURL);
        
        cleanedURL = getCleanedURL("Cavenders.js", "https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes");
        assertEquals("https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes", cleanedURL);
        
        cleanedURL = getCleanedURL("Cavenders.js", "https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes/western-square-toe-boots?viewall=true");
        assertEquals("https://www.cavenders.com/western/cowboy-boots/mens-boots-shoes/western-square-toe-boots", cleanedURL);
  
	}
}

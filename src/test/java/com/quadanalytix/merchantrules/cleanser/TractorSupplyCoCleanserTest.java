package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class TractorSupplyCoCleanserTest extends CleanserTest {

	@Test
    public void testTractorSupplyCoCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/catalog/pets-livestock/dog/dog-crates-carriers?cm_sp=Fly_Pets_Livestock-_-SubCategory-_-Dog+Dog_Crates_Carriers");
        assertEquals("http://www.tractorsupply.com/tsc/catalog/pets-livestock/dog/dog-crates-carriers", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/catalog/home-garden/outdoor-power-equipment/riding-zero-turn-mowers?cm_sp=Fly_Home_Garden-_-SubCategory-_-OPE+Riding_Zero_Turn_Mowers");
        assertEquals("http://www.tractorsupply.com/tsc/catalog/home-garden/outdoor-power-equipment/riding-zero-turn-mowers", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/catalog/footwear/womens-footwear/womens-western-footwear?cm_sp=Fly_Footwear-_-SubCategory-_-Womens_Footwear+Womens_Western_Footwear");
        assertNotEquals("http://www.tractorsupply.com/tsc/catalog/footwear/womens-footwear/womens-western-footwear?", cleanedURL3);       
	}
}
package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BelkCleanserTest extends CleanserTest{
	@Test
    public void testBelkCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/Juniors/Shop/Dresses.jsp#sO=&ZZ%3C%3EtP=4294922132%2B4294931431%2B4294941361%2B4294876819&ZZ_PO=0&ZZ_OPT=Y&numRecords=20&templateName=%2FAST%2FMain%2FBelk_Primary%2FJuniors%2FShop%2FDresses.jsp");
        assertEquals("http://www.belk.com/AST/Main/Belk_Primary/Juniors/Shop/Dresses.jsp", cleanedURL);
        
        cleanedURL = getCleanedURL("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/Handbags_And_Accessories/Shop/Accessories/Hats.jsp");
        assertEquals("http://www.belk.com/AST/Main/Belk_Primary/Handbags_And_Accessories/Shop/Accessories/Hats.jsp", cleanedURL);
        
        cleanedURL = getCleanedURL("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/Beauty_And_Fragrance/Shop/Fragrance/Mens/Gift_Sets.jsp#sO=&ZZ%3C%3EtP=4294923375%2B4294943425&ZZ_PO=0&ZZ_OPT=Y&numRecords=20&templateName=%2FAST%2FMain%2FBelk_Primary%2FBeauty_And_Fragrance%2FShop%2FFragrance%2FMens%2FGift_Sets.jsp&changeViewInd=y");
        assertEquals("http://www.belk.com/AST/Main/Belk_Primary/Beauty_And_Fragrance/Shop/Fragrance/Mens/Gift_Sets.jsp", cleanedURL);
        
        cleanedURL = getCleanedURL("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/Jewelry_And_Watches/Shop/Watches/Mens.jsp#sO=&ZZ%3C%3EtP=4294889032%2B132&ZZ_PO=0&ZZ_OPT=Y&numRecords=20&templateName=%2FAST%2FMain%2FBelk_Primary%2FJewelry_And_Watches%2FShop%2FWatches%2FMens.jsp&changeViewInd=y");
        assertEquals("http://www.belk.com/AST/Main/Belk_Primary/Jewelry_And_Watches/Shop/Watches/Mens.jsp", cleanedURL);
    }
}

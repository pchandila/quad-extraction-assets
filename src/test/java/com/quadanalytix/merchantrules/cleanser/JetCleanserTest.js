package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class JetCleanserTest extends CleanserTest {

	@Test
    public void testJetCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Jet.js", "https://jet.com/search?page=1&sort=price_low_to_high&category=9000213");
        assertEquals("https://jet.com/search?page=1&sort=price_low_to_high&category=9000213", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("Jet.js", "https://jet.com/search?category=15000004");
        assertEquals("https://jet.com/search?category=15000004", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("Jet.js", "https://jet.com/search?category=18000093");
        assertNotEquals("https://jet.com/search?category=18000093?", cleanedURL3);       
        
	}
}
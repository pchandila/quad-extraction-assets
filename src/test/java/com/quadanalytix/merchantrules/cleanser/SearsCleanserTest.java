package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class SearsCleanserTest extends CleanserTest {
    @Test
    public void testSearsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Sears.js", "http://www.sears.com/southpole-young-men-s-relaxed-jeans/p-043VA45450901P?prdNo=3");
        assertEquals("http://www.sears.com/southpole-young-men-s-relaxed-jeans/p-043VA45450901P", cleanedURL);
        
        cleanedURL = getCleanedURL("Sears.js", "http://www.sears.com/fitness-sports-fitness-exercise-ellipticals-accessories-elliptical-trainers/b-1340961555?previousSort=ORIGINAL_SORT_ORDER&pageNum=11&viewItems=50");
        assertEquals("http://www.sears.com/fitness-sports-fitness-exercise-ellipticals-accessories-elliptical-trainers/b-1340961555", cleanedURL);
    }
}

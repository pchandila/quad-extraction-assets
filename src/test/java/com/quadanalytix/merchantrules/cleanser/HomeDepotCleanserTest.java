package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class HomeDepotCleanserTest extends CleanserTest{
	@Test
    public void testGrouponGoodsCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("HomeDepot.js", "http://www.homedepot.com/b/Doors-Windows-Windows-Skylights-Vented-Skylights/N-5yc1vZc5g4");
        assertEquals("http://www.homedepot.com/b/Doors-Windows-Windows-Skylights-Vented-Skylights/N-5yc1vZc5g4", cleanedURL);
        
        cleanedURL = getCleanedURL("HomeDepot.js", "http://www.homedepot.com/b/Decor-Blinds-Window-Treatments-Curtains-Drapes/N-5yc1vZarfw?Nao=24");
        assertEquals("http://www.homedepot.com/b/Decor-Blinds-Window-Treatments-Curtains-Drapes/N-5yc1vZarfw", cleanedURL);
        
        cleanedURL = getCleanedURL("HomeDepot.js", "http://www.homedepot.com/b/Decor-Furniture-Bedroom-Furniture-Beds-Headboards/Home-Decorators-Collection/N-5yc1vZc7ohZ4vr?Nao=120");
        assertEquals("http://www.homedepot.com/b/Decor-Furniture-Bedroom-Furniture-Beds-Headboards/Home-Decorators-Collection/N-5yc1vZc7ohZ4vr", cleanedURL);
	}
}

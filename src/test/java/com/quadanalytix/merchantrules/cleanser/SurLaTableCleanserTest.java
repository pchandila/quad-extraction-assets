package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class SurLaTableCleanserTest extends CleanserTest {  
    @Test
    public void testSurLaTableCleanser() throws IOException {
        String cleanedURL = getCleanedURL("SurLaTable.js", "http://www.surlatable.com/category/SCA-5861/Kitchen+Shears?cleanSession=true&pCat=SPC-13115");
        assertEquals("http://www.surlatable.com/category/SCA-5861/Kitchen+Shears", cleanedURL);
        
        cleanedURL = getCleanedURL("SurLaTable.js", "http://www.surlatable.com/category/SCA-5855/Cleavers+and+Boning+Knives?cleanSession=true&pCat=CAT-5781");
        assertEquals("http://www.surlatable.com/category/SCA-5855/Cleavers+and+Boning+Knives", cleanedURL);
    }
}

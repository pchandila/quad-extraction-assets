package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class WestElmCleanserTest extends CleanserTest {  
    @Test
    public void testSurLaTableCleanser() throws IOException {
        String cleanedURL = getCleanedURL("WestElm.js", "http://www.westelm.com/shop/bath/countertop-toiletry-bags/?cm_type=gnav");
        assertEquals("http://www.westelm.com/shop/bath/countertop-toiletry-bags/", cleanedURL);
        
        cleanedURL = getCleanedURL("WestElm.js", "http://www.westelm.com/shop/bedding/bedding-sets/?");
        assertEquals("http://www.westelm.com/shop/bedding/bedding-sets/", cleanedURL);
    }
}

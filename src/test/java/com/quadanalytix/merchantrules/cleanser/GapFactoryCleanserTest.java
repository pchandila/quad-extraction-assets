package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class GapFactoryCleanserTest extends CleanserTest{
	@Test
    public void GapFactoryCleanser() throws IOException {
        String cleanedURL = getCleanedURL("GapFactory.js", "http://www.gapfactory.com/browse/category.do?cid=1049456#pageId=0&size=3-1:19,18,17,16,165&price=9-12");
        assertEquals("http://www.gapfactory.com/browse/category.do?cid=1049456", cleanedURL);
        
        cleanedURL = getCleanedURL("GapFactory.js", "http://www.gapfactory.com/browse/category.do?cid=1041610");
        assertEquals("http://www.gapfactory.com/browse/category.do?cid=1041610", cleanedURL);
    }
}

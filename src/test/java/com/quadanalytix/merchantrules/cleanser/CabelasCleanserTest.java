package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CabelasCleanserTest extends CleanserTest{
	@Test
    public void testCabelasCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Cabelas.js", "http://www.cabelas.com/catalog/browse/mens-long-sleeve-lightweight-casual-shirts/_/N-1100832+10005032/Ne-10005032?WTz_st=GuidedNav&WTz_stype=GNU");
        assertEquals("http://www.cabelas.com/catalog/browse/mens-long-sleeve-lightweight-casual-shirts/_/N-1100832", cleanedURL);
        
        cleanedURL = getCleanedURL("Cabelas.js", "http://www.cabelas.com/catalog/browse/flotation-sale/_/N-1116834+10000047/Ne-10000047?WTz_st=GuidedNav&WTz_stype=GNU");
        assertEquals("http://www.cabelas.com/catalog/browse/flotation-sale/_/N-1116834", cleanedURL);
        
        cleanedURL = getCleanedURL("Cabelas.js", "http://www.cabelas.com/catalog/browse/boat-seat-pedestal-and-base-sale/_/N-1122159+10005236/Ne-10005236?WTz_st=GuidedNav&WTz_stype=GNU");
        assertEquals("http://www.cabelas.com/catalog/browse/boat-seat-pedestal-and-base-sale/_/N-1122159", cleanedURL);
        
        cleanedURL = getCleanedURL("Cabelas.js", "http://www.cabelas.com/catalog/browse.cmd?N=1100502");
        assertEquals("http://www.cabelas.com/catalog/browse.cmd?N=1100502", cleanedURL);
    }
}

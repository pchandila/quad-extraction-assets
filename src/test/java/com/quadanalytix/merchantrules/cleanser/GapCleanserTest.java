package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class GapCleanserTest extends CleanserTest {
    @Test
    public void testGapCleanser() throws IOException {
    
        String cleanedURL = getCleanedURL("Gap.js", "http://www.gap.com/browse/category.do?cid=65289&departmentRedirect=true#department=75");
        assertEquals("http://www.gap.com/browse/category.do?cid=65289", cleanedURL);
    	
    }
}

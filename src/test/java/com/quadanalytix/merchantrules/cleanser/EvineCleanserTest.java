package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class EvineCleanserTest extends CleanserTest {
    
	@Test
    public void testEvineCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("EVINE.js", "http://www.evine.com/b/fashion/matisse-footwear/?r=ValuePays|5+ValuePay+and+up&r=price|10.00-200.00&p=4");
        assertEquals("http://www.evine.com/b/fashion/matisse-footwear/?r=ValuePays|5+ValuePay+and+up", cleanedURL);
        
        cleanedURL = getCleanedURL("EVINE.js", "http://www.evine.com/v/deadliest-catch/?icid=DN-KitchenFood-Brand_3-5-DeadliestCatch&r=price|14.00-100.00&p=1");
        assertEquals("http://www.evine.com/v/deadliest-catch/?icid=DN-KitchenFood-Brand_3-5-DeadliestCatch", cleanedURL);
        
        cleanedURL = getCleanedURL("EVINE.js", "http://www.evine.com/b/electronics/asus/?icid=DN-Electronics-Brand_3-2-ASUS");
        assertEquals("http://www.evine.com/b/electronics/asus/?icid=DN-Electronics-Brand_3-2-ASUS", cleanedURL);
        
        cleanedURL = getCleanedURL("EVINE.js", "http://www.evine.com/v/deadliest-catch/?icid=DN-KitchenFood-Brand_3-5-DeadliestCatch&r=price|14.00-100.00");
        assertEquals("http://www.evine.com/v/deadliest-catch/?icid=DN-KitchenFood-Brand_3-5-DeadliestCatch", cleanedURL);
        
        cleanedURL = getCleanedURL("EVINE.js", "http://www.evine.com/v/deadliest-catch/?r=price|14.00-100.00&p=1");
        assertEquals("http://www.evine.com/v/deadliest-catch/", cleanedURL);
	}
}

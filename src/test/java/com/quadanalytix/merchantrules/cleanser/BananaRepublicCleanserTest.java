package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BananaRepublicCleanserTest extends CleanserTest {
    @Test
    public void testBananaRepublicCleanser() throws IOException {
        String cleanedURL = getCleanedURL("BananaRepublic.js", "http://bananarepublic.gap.com/browse/category.do?cid=1014739&mlink=1014329,10685129,SDP_W&clink=10685129");
        assertEquals("http://bananarepublic.gap.com/browse/category.do?cid=1014739", cleanedURL); 
        cleanedURL = getCleanedURL("BananaRepublic.js", "http://bananarepublic.gap.com/browse/category.do?cid=1036541&departmentRedirect=true#department=75");
        assertEquals("http://bananarepublic.gap.com/browse/category.do?cid=1036541", cleanedURL); 
        
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class LLBeanCleanserTest extends CleanserTest {
    @Test
    public void testLLBeanCleanser() throws IOException {
        String cleanedURL = getCleanedURL("LLBean.js", "http://www.llbean.com/webapp/wcs/stores/servlet/GuidednavDisplay?categoryId=610&catalogId=1&position=-1&page=outerwear&nav=pg2-610&start=49");
        assertEquals("http://www.llbean.com/webapp/wcs/stores/servlet/GuidednavDisplay?categoryId=610&catalogId=1&position=-1&page=outerwear", cleanedURL);
        
        cleanedURL = getCleanedURL("LLBean.js", "http://www.llbean.com/llb/shop/622?page=womens-pants-and-capris&nav=gnro-hp");
        assertEquals("http://www.llbean.com/llb/shop/622?page=womens-pants-and-capris", cleanedURL);
        
        cleanedURL = getCleanedURL("LLBean.js", "http://www.llbean.com/llb/shop/513283?nav=ln-513283&sort_field=Grade+%28Descending%29&start=1");
        assertEquals("http://www.llbean.com/llb/shop/513283", cleanedURL);
    }
}
package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class PetcoCleanserTest extends CleanserTest {
    @Test
    public void testPetcoCleanser() throws IOException {
    
    	String cleanedURL2 = getCleanedURL("Petco.js", "http://www.petco.com/shop/en/petcostore/fish/fish-aquariums-kits");
        assertEquals("http://www.petco.com/shop/en/petcostore/fish/fish-aquariums-kits", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("Petco.js", "http://www.petco.com/shop/en/petcostore/fish/fish-aquariums-kits");
        assertNotEquals("http://www.petco.com/shop/en/petcostore/fish/fish-aquariums-kits#", cleanedURL2);
    
    	String cleanedURL3 = getCleanedURL("Petco.js", "http://www.petco.com/shop/en/petcostore/bird/bird-cages-and-accessories#facet:-1002653869326797103101326711110911297110121&productBeginIndex:48&orderBy:&pageView:&minPrice:&maxPrice:&pageSize:24&");
        assertEquals("http://www.petco.com/shop/en/petcostore/bird/bird-cages-and-accessories#facet:-1002653869326797103101326711110911297110121", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("Petco.js", "http://www.petco.com/shop/en/petcostore/bird/bird-cages-and-accessories#facet:-1002653869326797103101326711110911297110121&productBeginIndex:48&orderBy:&pageView:&minPrice:&maxPrice:&pageSize:24&");
        assertNotEquals("http://www.petco.com/shop/en/petcostore/bird/bird-cages-and-accessories#facet:-1002653869326797103101326711110911297110121&productBeginIndex:48&orderBy:&pageView:&minPrice:&maxPrice:&pageSize:24&", cleanedURL3);
    
    }
}

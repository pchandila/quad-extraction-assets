package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class PFIWesternWearStoreCleanserTest extends CleanserTest {

	@Test
    public void testPFIWesternWearStoreCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("PFIWesternWearStore.js", "http://www.pfiwestern.com/womens/womens-western-clothing/womens-shirts/blouses-shirts.html?dir=asc&order=name");
        assertEquals("http://www.pfiwestern.com/womens/womens-western-clothing/womens-shirts/blouses-shirts.html", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("PFIWesternWearStore.js", "http://www.pfiwestern.com/kids/new-arrivals.html?dir=asc&limit=66&order=price");
        assertEquals("http://www.pfiwestern.com/kids/new-arrivals.html", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("PFIWesternWearStore.js", "http://www.pfiwestern.com/womens/womens-western-clothing/western-dresses-skirts.html?dir=asc&limit=99&order=name");
        assertNotEquals("http://www.pfiwestern.com/womens/womens-western-clothing/western-dresses-skirts.html?", cleanedURL3);       
	}
}
package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class AcademySportsCleanserTest extends CleanserTest{
	@Test
    public void testAcademySportsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("AcademySports.js", "http://www.academy.com/shop/browse/shops/stay-warm/fleece-jackets?searchType=2&searchTerm=&cm_cr=No+Campaign-_-Web+Activity-_-Holidays+%26+Events-_-SubCategory_CP1-_-Fleece+Vests+%26+Jackets-productImageLink&beginIndex=0&orderBy=&pageView=grid&minPrice=&maxPrice=&pageSize=&");
        assertEquals("http://www.academy.com/shop/browse/shops/stay-warm/fleece-jackets", cleanedURL);
        
        cleanedURL = getCleanedURL("AcademySports.js", "http://www.academy.com/shop/browse/apparel/mens-apparel/mens-shirts--t-shirts?searchType=2&searchTerm=&beginIndex=0&orderBy=&pageView=grid&minPrice=&maxPrice=&pageSize=&");
        assertEquals("http://www.academy.com/shop/browse/apparel/mens-apparel/mens-shirts--t-shirts", cleanedURL);
        
        cleanedURL = getCleanedURL("AcademySports.js", "http://www.academy.com/shop/browse/apparel/mens-apparel/mens-shirts--t-shirts");
        assertEquals("http://www.academy.com/shop/browse/apparel/mens-apparel/mens-shirts--t-shirts", cleanedURL);
    }
}

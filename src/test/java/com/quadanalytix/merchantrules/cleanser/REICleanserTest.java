package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class REICleanserTest extends CleanserTest {
    @Test
    public void testReiCleanser() throws IOException {
    
    	String cleanedURL2 = getCleanedURL("REI.js", "http://www.rei.com/search.html?ir=q%3Ashoes+for+cycling&page=1&q=shoes+for+cycling");
        assertEquals("http://www.rei.com/search.html?q=shoes+for+cycling", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("REI.js", "http://www.rei.com/search.html?ir=q%3Ashoes+for+cycling&page=1&q=shoes+for+cycling");
        assertNotEquals("http://www.rei.com/search.html?ir=q%3Ashoes+for+cycling&page=1&q=shoes+for+cycling", cleanedURL2);
    
    	String cleanedURL3 = getCleanedURL("REI.js", "http://www.rei.com/c/mens-road-running-shoes?ir=category%3Amens-road-running-shoes&r=c&page=1");
        assertEquals("http://www.rei.com/c/mens-road-running-shoes", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("REI.js", "http://www.rei.com/c/mens-road-running-shoes?ir=category%3Amens-road-running-shoes&r=c&page=1");
        assertNotEquals("http://www.rei.com/c/mens-road-running-shoes?ir=category%3Amens-road-running-shoes&r=c&page=1", cleanedURL3);
    
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class SmartPakEquineCleanserTest extends CleanserTest {
    @Test
    public void testSmartPakEquineCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("SmartPakEquine.js", "https://www.smartpakequine.com/search/search?searchTerm=top");
        assertEquals("https://www.smartpakequine.com/search/search?searchTerm=top", cleanedURL);
        String cleanedURL1 = getCleanedURL("SmartPakEquine.js", "https://www.smartpakequine.com/search/Searcha?parameters=%2Fi%2F1%2Fq%2Ftop%2Fsegment%2Fproduct%2Fsort%2Fmin_sale_price");
        assertEquals("https://www.smartpakequine.com/search/Searcha?parameters=%2Fi%2F1%2Fq%2Ftop%2Fsegment%2Fproduct%2Fsort%2Fmin_sale_price", cleanedURL1);
        
        
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class DoverSaddleryCleanserTest extends CleanserTest{
	@Test
    public void testCabelasCleanser() throws IOException {
        String cleanedURL = getCleanedURL("DoverSaddlery.js", "http://www.doversaddlery.com/closeouts/c/8000/filter/100000008226eq100000010310/");
        assertEquals("http://www.doversaddlery.com/closeouts/c/8000/filter/100000008226eq100000010310/", cleanedURL);
        
        cleanedURL = getCleanedURL("DoverSaddlery.js", "http://www.doversaddlery.com/dog-blankets/c/4601/");
        assertEquals("http://www.doversaddlery.com/dog-blankets/c/4601/", cleanedURL);
    }
}

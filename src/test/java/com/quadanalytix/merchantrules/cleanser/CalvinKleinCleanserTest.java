package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CalvinKleinCleanserTest extends CleanserTest {
    @Test
    public void testCalvinKleinCleanser() throws IOException {
        String cleanedURL = getCleanedURL("CalvinKlein.js", "http://www.calvinklein.com/shop/en/ck/search/mens-tees-polos-sale");
        assertEquals("http://www.calvinklein.com/shop/en/ck/search/mens-tees-polos-sale", cleanedURL);
        
        cleanedURL = getCleanedURL("CalvinKlein.js", "www.calvinklein.com/shop/en/ck/search/mens-shoes?ab=bags+shoes_2&cd=123");
        assertEquals("www.calvinklein.com/shop/en/ck/search/mens-shoes?cd=123", cleanedURL);
        
        cleanedURL = getCleanedURL("CalvinKlein.js", "www.calvinklein.com/shop/en/ck/search/mens-shoes?ab=bags+shoes_2");
        assertEquals("www.calvinklein.com/shop/en/ck/search/mens-shoes", cleanedURL);
    }
}

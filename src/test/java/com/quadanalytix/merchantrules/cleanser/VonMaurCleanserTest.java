package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class VonMaurCleanserTest extends CleanserTest {
    @Test
    public void testVonMaurCleanser() throws IOException {
    
        String cleanedURL = getCleanedURL("VonMaur.js", "http://vonmaur.com/Results.aspx?md=c&cat=646,55&st=1&pp=20&pg=68");
        assertEquals("http://vonmaur.com/Results.aspx?md=c&cat=646,55", cleanedURL);
        
        cleanedURL = getCleanedURL("VonMaur.js", "http://vonmaur.com/Results.aspx?md=c&cat=645,312&st=6&pp=20&pg=1");
        assertEquals("http://vonmaur.com/Results.aspx?md=c&cat=645,312", cleanedURL);
        
        cleanedURL = getCleanedURL("VonMaur.js", "http://vonmaur.com/Results.aspx?md=c&cat=646,540&st=1&pp=100&pg=1");
        assertEquals("http://vonmaur.com/Results.aspx?md=c&cat=646,540", cleanedURL);
    	
    }
}

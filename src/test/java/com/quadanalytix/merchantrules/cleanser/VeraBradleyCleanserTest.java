package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class VeraBradleyCleanserTest extends CleanserTest {
    @Test
    public void testVeraBradleyCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("VeraBradley.js", "http://www.verabradley.com/section/bags.uts?N=30000&Ns=3");
        assertEquals("http://www.verabradley.com/section/bags.uts", cleanedURL);
        
        String cleanedURL3 = getCleanedURL("VeraBradley.js", "http://www.verabradley.com/category/promotions/wallets-and-wristlets-sale.uts");
        assertEquals("http://www.verabradley.com/category/promotions/wallets-and-wristlets-sale.uts", cleanedURL3);
        
        String cleanedURL1 = getCleanedURL("VeraBradley.js", "http://www.verabradley.com/section/accessories.uts?N=10000&Ns=3");
        assertEquals("http://www.verabradley.com/section/accessories.uts", cleanedURL1);
    	
        String cleanedURL4 = getCleanedURL("VeraBradley.js", "http://www.verabradley.com/section/bags.uts?N=30000&Ns=3");
        assertNotEquals("http://www.verabradley.com/section/bags.uts?", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class AmericanEagleOutfittersCleanserTest extends CleanserTest{
	@Test
    public void testAmericanEagleOutfittersCleanser() throws IOException {
        String cleanedURL = getCleanedURL("AmericanEagleOutfitters.js", "https://www.ae.com/aerie-bath-body-beauty/aerie/s-cat/7670086?cm=sUS-cUSD");
        assertEquals("https://www.ae.com/aerie-bath-body-beauty/aerie/s-cat/7670086", cleanedURL);
        
        cleanedURL = getCleanedURL("AmericanEagleOutfitters.js", "https://www.ae.com/men-crew-neck-t-shirts/web/s-cat/6470437/?cm=sUS-cUSD&color=black&N=4294960727");
        assertEquals("https://www.ae.com/men-crew-neck-t-shirts/web/s-cat/6470437/", cleanedURL);
        
        cleanedURL = getCleanedURL("AmericanEagleOutfitters.js", "https://www.ae.com/men-crew-neck-t-shirts/web/s-cat/6470437/?cm=sUS-cUSD&size=l-tall&N=4294966722");
        assertEquals("https://www.ae.com/men-crew-neck-t-shirts/web/s-cat/6470437/", cleanedURL);
        
        cleanedURL = getCleanedURL("AmericanEagleOutfitters.js", "https://www.ae.com/women-skinny-jeans/web/s-cat/1990002?cm=sUS-cUSD&guide=jean&navdetail=mega:cat7010140:c1:p3");
        assertEquals("https://www.ae.com/women-skinny-jeans/web/s-cat/1990002", cleanedURL);
    }
}

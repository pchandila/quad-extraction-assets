package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class BarnesNobleCleanserTest extends CleanserTest{
	@Test
    public void testBarnesNobleCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("BarnesNoble.js", "http://www.barnesandnoble.com/b/books/self-help-relationships/_/N-26Z29Z8q8Z18ly;jsessionid=2F605840963C33F04242D0A2E8DD7365.prodny_store02-atgap13?Ns=P_Sales_Rank");
        assertEquals("http://www.barnesandnoble.com/b/books/self-help-relationships/_/N-26Z29Z8q8Z18ly", cleanedURL);
        
        cleanedURL = getCleanedURL("BarnesNoble.js", "http://www.barnesandnoble.com/b/books/computers/_/N-1fZ29Z8q8Zug4;jsessionid=5E44D601703E6E1139106FA24E5C3A23.prodny_store02-atgap12?Ns=P_Sales_Rank");
        assertEquals("http://www.barnesandnoble.com/b/books/computers/_/N-1fZ29Z8q8Zug4", cleanedURL);
        
        cleanedURL = getCleanedURL("BarnesNoble.js", "http://www.barnesandnoble.com/b/textbooks/science-technology/engineering/_/N-8q9Z2bxy");
        assertEquals("http://www.barnesandnoble.com/b/textbooks/science-technology/engineering/_/N-8q9Z2bxy", cleanedURL);
  
	}
}

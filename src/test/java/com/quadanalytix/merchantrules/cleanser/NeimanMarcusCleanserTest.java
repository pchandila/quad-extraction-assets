package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class NeimanMarcusCleanserTest extends CleanserTest{
	@Test
    public void testNeimanMarcusCleanser() throws IOException {
        String cleanedURL = getCleanedURL("NeimanMarcus.js", "http://www.neimanmarcus.com/Fendi/Handbags/cat14170738_cat12890741_cat44690739/c.cat?menuPath=cat44690739_cat12890741_cat14170738&navAction=menu");
        assertEquals("http://www.neimanmarcus.com/Fendi/Handbags/cat14170738_cat12890741_cat44690739/c.cat", cleanedURL);
        
        cleanedURL = getCleanedURL("NeimanMarcus.js", "http://www.neimanmarcus.com/Shoes/Dress-Shoes/cat50070779_cat000550_cat000470/c.cat#userConstrainedResults=true&refinements=717,719&page=1&pageSize=30&sort=PCS_SORT&definitionPath=/nm/commerce/pagedef_rwd/template/EndecaDrivenHome&locationInput=PORTLAND+,+OR&radiusInput=100&onlineOnly=online&allStoresInput=false&rwd=true&catalogId=cat50070779");
        assertEquals("http://www.neimanmarcus.com/Shoes/Dress-Shoes/cat50070779_cat000550_cat000470/c.cat", cleanedURL);
        
        cleanedURL = getCleanedURL("NeimanMarcus.js", "http://www.neimanmarcus.com/Chloe/Shoes/cat7400732_cat10090734_cat44690737/c.cat");
        assertEquals("http://www.neimanmarcus.com/Chloe/Shoes/cat7400732_cat10090734_cat44690737/c.cat", cleanedURL);
        
        cleanedURL = getCleanedURL("NeimanMarcus.js", "http://www.neimanmarcus.com/Chloe/Shoes/cat7400732_cat10090734_cat44690737/c.cat#userConstrainedResults=true&refinements=&page=3&pageSize=30&sort=&definitionPath=/nm/commerce/pagedef_rwd/template/EndecaDrivenHome&locationInput=&radiusInput=100&onlineOnly=&allStoresInput=false&rwd=true&catalogId=cat7400732");
        assertEquals("http://www.neimanmarcus.com/Chloe/Shoes/cat7400732_cat10090734_cat44690737/c.cat", cleanedURL);
    }
}

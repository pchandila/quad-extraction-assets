package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class PierOneCleanserTest extends CleanserTest {
    @Test
    public void testPierOneCleanser() throws IOException {
        String cleanedURL1 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/bedroom-furniture,default,sc.html?sz=60&start=0#nav=left");
        assertEquals("http://www.pier1.com/furniture/bedroom-furniture,default,sc.html?sz=60&start=0#nav=left", cleanedURL1);
        
        cleanedURL1 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/bedroom-furniture,default,sc.html?sz=60&start=0#nav=left");
        assertNotEquals("http://www.pier1.com/furniture/bedroom-furniture,default,sc.html?sz=60", cleanedURL1);
    	
    	String cleanedURL2 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/living-room/accent-tables/coffee-tables,default,sc.html?icid=cat_indoor_seating-subcat_accent_tables-subcat_tile_coffee_tables&nav=tile");
        assertEquals("http://www.pier1.com/furniture/living-room/accent-tables/coffee-tables,default,sc.html?icid=cat_indoor_seating-subcat_accent_tables-subcat_tile_coffee_tables", cleanedURL2);
        
        cleanedURL2 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/living-room/accent-tables/coffee-tables,default,sc.html?icid=cat_indoor_seating-subcat_accent_tables-subcat_tile_coffee_tables&nav=tile");
        assertNotEquals("http://www.pier1.com/furniture/living-room/accent-tables/coffee-tables,default,sc.html?icid=cat_indoor_seating-subcat_accent_tables-subcat_tile_coffee_tables&nav=tile", cleanedURL2);
    
    	String cleanedURL3 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/bedroom-furniture/beds-daybeds,default,sc.html#nav=top");
        assertEquals("http://www.pier1.com/furniture/bedroom-furniture/beds-daybeds,default,sc.html#nav=top", cleanedURL3);
        
        cleanedURL3 = getCleanedURL("PierOne.js", "http://www.pier1.com/furniture/bedroom-furniture/beds-daybeds,default,sc.html#nav=top");
        assertNotEquals("http://www.pier1.com/furniture/bedroom-furniture/beds-daybeds,default,sc.html", cleanedURL3);
    
    }
}

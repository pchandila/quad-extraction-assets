package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class UrbanOutfittersCleanserTest extends CleanserTest {
    @Test
    public void testUrbanOutfittersCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("UrbanOutfitters.js", "http://www.urbanoutfitters.com/urban/catalog/category.jsp?id=M_APP_POLOS#/");
        assertEquals("http://www.urbanoutfitters.com/urban/catalog/category.jsp?id=M_APP_POLOS#/", cleanedURL);
        String cleanedURL1 = getCleanedURL("UrbanOutfitters.js", "http://www.urbanoutfitters.com/urban/catalog/category.jsp?id=W-NEW-DRESSES&cm_sp=WOMENS-_-L3-_-W_NEWARRIVALS:W-NEW-DRESSES#/");
        assertEquals("http://www.urbanoutfitters.com/urban/catalog/category.jsp?id=W-NEW-DRESSES&cm_sp=WOMENS-_-L3-_-W_NEWARRIVALS:W-NEW-DRESSES#/", cleanedURL1);
    }
}

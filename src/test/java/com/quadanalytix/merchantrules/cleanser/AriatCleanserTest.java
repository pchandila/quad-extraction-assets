package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class AriatCleanserTest extends CleanserTest{
	@Test
    public void testAriatCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Ariat.js", "http://www.ariat.com/mens-western-clearance#prefn1=Type_Filter&prefn2=color&prefv1=Boot%20Cut&prefv2=Blue");
        assertEquals("http://www.ariat.com/mens-western-clearance", cleanedURL);
        
        cleanedURL = getCleanedURL("Ariat.js", "http://www.ariat.com/mens-western-clearance");
        assertEquals("http://www.ariat.com/mens-western-clearance", cleanedURL);
        
        cleanedURL = getCleanedURL("Ariat.js", "http://www.ariat.com/men-western-apparel-tops");
        assertEquals("http://www.ariat.com/men-western-apparel-tops", cleanedURL);
    }
}

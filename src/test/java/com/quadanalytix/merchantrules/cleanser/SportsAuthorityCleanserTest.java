package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class SportsAuthorityCleanserTest extends CleanserTest {
    @Test
    public void testSearsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("SportsAuthority.js", "http://www.sportsauthority.com/Fitness-Exercise-Equipment/Cardio-Equipment/Stair-Steppers/family.jsp?categoryId=11925657&cp=3077568.45510056");
        assertEquals("http://www.sportsauthority.com/Fitness-Exercise-Equipment/Cardio-Equipment/Stair-Steppers/family.jsp?categoryId=11925657", cleanedURL);
        
        cleanedURL = getCleanedURL("SportsAuthority.js", "http://www.sportsauthority.com/Team-Sports/Football/Receiver-Lineman-Gloves/family.jsp?categoryId=2360707&cp=3077571.64567426");
        assertEquals("http://www.sportsauthority.com/Team-Sports/Football/Receiver-Lineman-Gloves/family.jsp?categoryId=2360707", cleanedURL);
    }
}

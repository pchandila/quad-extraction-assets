package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class DSWCleanserTest extends CleanserTest {
    @Test
    public void testDSWCleanser() throws IOException {
//    
        String cleanedURL = getCleanedURL("DSW.js", "http://www.dsw.com/Womens-Shoes-Sandals/_/N-272u?Ns=nonMemberPrice|0");
        assertEquals("http://www.dsw.com/Womens-Shoes-Sandals/_/N-272u", cleanedURL);
        String cleanedURL1 = getCleanedURL("DSW.js", "http://www.dsw.com/shoe-brands/I-Heart-UGG/_/N-1z13spzZ1z13z66Z1z13yrr?activeBrand=4294955303+4294963662+4294963143");
        assertEquals("http://www.dsw.com/shoe-brands/I-Heart-UGG/_/N-1z13spzZ1z13z66Z1z13yrr", cleanedURL1);
        String cleanedURL2 = getCleanedURL("DSW.js", "http://www.dsw.com/Clearance-Mens-Boots/_/N-278z?Ns=product.daysAvailable|0%3Aproduct.inventory|1");
        assertEquals("http://www.dsw.com/Clearance-Mens-Boots/_/N-278z", cleanedURL2);
        
    }
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class PotteryBarnKidsCleanserTest extends CleanserTest {
    @Test
    public void testPotteryBarnKidsCleanser() throws IOException {
        String cleanedURL = getCleanedURL("PotteryBarnKids.js", "http://www.potterybarnkids.com/search/new/results.html?N=4294960509&cm_sp=HeaderLinks-_-OnsiteSearch-_-MainSite&cm_type=OnsiteSearch&typeahead=legacy&words=activity+table");
        assertEquals("http://www.potterybarnkids.com/search/new/results.html?N=4294960509&words=activity+table", cleanedURL);
        
        cleanedURL = getCleanedURL("PotteryBarnKids.js", "http://www.potterybarnkids.com/search/new/results.html?N=4294960509&cm_sp=HeaderLinks-_-OnsiteSearch-_-MainSite&cm_type=OnsiteSearch&typeahead=legacy&words=activity+table");
        assertNotEquals("http://www.potterybarnkids.com/search/new/results.html", cleanedURL);
        
        cleanedURL = getCleanedURL("PotteryBarnKids.js", "http://www.potterybarnkids.com/shop/baby/furniture/changing-tables/?cm_type=gnav");
        assertEquals("http://www.potterybarnkids.com/shop/baby/furniture/changing-tables/", cleanedURL);
        
        cleanedURL = getCleanedURL("PotteryBarnKids.js", "http://www.potterybarnkids.com/shop/baby/furniture/changing-tables/?cm_type=gnav");
        assertNotEquals("http://www.potterybarnkids.com/shop/baby/furniture/changing-tables/?", cleanedURL);
    }
}

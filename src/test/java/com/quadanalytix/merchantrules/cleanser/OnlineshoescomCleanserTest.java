package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;

import org.junit.Test;

public class OnlineshoescomCleanserTest extends CleanserTest {

	@Test
    public void testOnlineshoescomCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("Onlineshoescom.js", "http://www.onlineshoes.com/bags-list_ct2/1/60/relevance");
        assertEquals("http://www.onlineshoes.com/bags-list_ct2/1/60/relevance", cleanedURL);
        
        String cleanedURL2 = getCleanedURL("Onlineshoescom.js", "http://www.onlineshoes.com/mens-clothing-socks-list_ct159/1/60");
        assertEquals("http://www.onlineshoes.com/mens-clothing-socks-list_ct159/1/60", cleanedURL2);
        
        String cleanedURL3 = getCleanedURL("Onlineshoescom.js", "http://www.onlineshoes.com/product-list/1/60/");
        assertNotEquals("http://www.onlineshoes.com/product-list/1/60/?", cleanedURL3);       
	}
}
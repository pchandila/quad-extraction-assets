package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class RalphLaurenCleanserTest extends CleanserTest{
	@Test
    public void testRalphLaurenCleanser() throws IOException {
		
		String cleanedURL = getCleanedURL("RalphLauren.js", "http://www.ralphlauren.com/family/index.jsp?categoryId=2877586&cp=1760781&ab=ln_men_cs_belts");
        assertEquals("http://www.ralphlauren.com/family/index.jsp?categoryId=2877586&cp=1760781&ab=ln_men_cs_belts", cleanedURL);
        
        cleanedURL = getCleanedURL("RalphLauren.js", "http://www.ralphlauren.com/family/index.jsp?fd=Ralph+Lauren&ff=Brand&fg=Brand&fv=1000037%2FRalph+Lauren&fg=Color&ff=GenericColor&fv=Black&fd=Black&cp=1760781&categoryId=2877586&ab=ln_men_cs_belts&braces=true");
        assertEquals("http://www.ralphlauren.com/family/index.jsp?cp=1760781&categoryId=2877586&ab=ln_men_cs_belts", cleanedURL);
        
        cleanedURL = getCleanedURL("RalphLauren.js", "http://www.ralphlauren.com/family/index.jsp?fd=Tall+3L&ff=SizeGeneral&fg=Size&fv=SQ000000528QSTall+3L&fd=S&ff=SizeGeneral&fg=Size&fv=SQ000000602QSS&fd=M&ff=SizeGeneral&fg=Size&fv=SQ000000603QSM&cp=1760781&categoryId=1760809&ab=ln_men_cs_poloshirts");
        assertEquals("http://www.ralphlauren.com/family/index.jsp?cp=1760781&categoryId=1760809&ab=ln_men_cs_poloshirts", cleanedURL);
        
	}
}

package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class CasaCleanserTest extends CleanserTest {
    @Test
    public void testCasaCleanser() throws IOException {
        String cleanedURL = getCleanedURL("Casa.js", "http://www.casa.com/subcat=Small-Appliances-10751/Casa+Type=Coffee+-and-+Tea+Appliances?PageIndex=1");
        assertEquals("http://www.casa.com/subcat=Small-Appliances-10751/Casa+Type=Coffee+-and-+Tea+Appliances", cleanedURL);
        
        cleanedURL = getCleanedURL("Casa.js", "http://www.casa.com/subcat=Knives-Cutlery-10752/Casa+Type=Cheese+Knives+-and-+Spreaders");
        assertEquals("http://www.casa.com/subcat=Knives-Cutlery-10752/Casa+Type=Cheese+Knives+-and-+Spreaders", cleanedURL);
        
        cleanedURL = getCleanedURL("Casa.js", "http://www.casa.com/subcat=Coffee-Tea-12416/Casa+Type=Coffee+-and-+Tea+Appliances/Refine+by+Type=Electric+Coffee+Makers%7CPercolators");
        assertEquals("http://www.casa.com/subcat=Coffee-Tea-12416/Casa+Type=Coffee+-and-+Tea+Appliances", cleanedURL);
        
        cleanedURL = getCleanedURL("Casa.js", "http://www.casa.com/subcat=Coffee-Tea-12416/Casa+Type=Coffee+-and-+Tea+Appliances/Refine+by+Type=Electric+Coffee+Makers%7CPercolators?PageIndex=1");
        assertEquals("http://www.casa.com/subcat=Coffee-Tea-12416/Casa+Type=Coffee+-and-+Tea+Appliances", cleanedURL);
    }
}

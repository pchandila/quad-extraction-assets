package com.quadanalytix.merchantrules.cleanser;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

public class OldNavyCleanserTest extends CleanserTest {
    @Test
    public void testOldNavyCleanser() throws IOException {
        String cleanedURL = getCleanedURL("OldNavy.js", "http://oldnavy.gap.com/browse/category.do?cid=1038317");
        assertEquals("http://oldnavy.gap.com/browse/category.do?cid=1038317", cleanedURL);
        
        cleanedURL = getCleanedURL("OldNavy.js", "http://oldnavy.gap.com/browse/category.do?cid=1007068&mlink=5151,10874875,Splash_Active_2_g_bkgd&clink=10874875");
        assertEquals("http://oldnavy.gap.com/browse/category.do?cid=1007068", cleanedURL);
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PacSunDeduperTest extends DeduperTest {
    @Test
    public void testPacSunDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PacSun.js", "http://www.pacsun.com/pacsun/erik-scallop-t-shirt-0120250500007.html?cgid=mens-graphic-tees&prefn1=special-offers&dwvar_0120250500007_color=040&start=3&prefv1=2%20for%20%2430&dwvar_0120250500007_size=9200");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("erik-scallop-t-shirt-0120250500007.html", uniquesAttrs.get(0));
    }
}

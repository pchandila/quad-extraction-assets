package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TractorSupplyCoDeduperTest extends DeduperTest {
    @Test
    public void testTractorSupplyCoDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/product/cub-cadet-xt1-enduro-series-46-in-22-hp-v-twin-hydrostatic-riding-mower");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("cub-cadet-xt1-enduro-series-46-in-22-hp-v-twin-hydrostatic-riding-mower", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/product/winchester-26-gun-granite-safe-with-lighted-ul-electronic-lock");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("winchester-26-gun-granite-safe-with-lighted-ul-electronic-lock", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("TractorSupplyCo.js", "http://www.tractorsupply.com/tsc/product/carhartt-ladies-sandstone-berkley-vest-ii");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("carhartt-ladies-sandstone-berkley-vest-ii", uniquesAttrs3.get(0));
        
    }
}

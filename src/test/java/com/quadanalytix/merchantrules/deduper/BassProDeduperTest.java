package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BassProDeduperTest extends DeduperTest {
    @Test
    public void testBassProDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("BassPro.js", "http://www.basspro.com/Lews-BB2-Wide-Speed-Spool-Baitcast-Reel/product/2291977/");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("2291977", uniquesAttrs.get(0));
    }
      
    @Test
    public void randomURLCheck() throws IOException, URISyntaxException {
       final List<String> uniquesAttrs = getUniqueAttributes("BassPro.js", "http//:www.google.com");
       assertEquals(0, uniquesAttrs.size());
      }
    
}

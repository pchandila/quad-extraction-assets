package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BananaRepublicFactoryDeduperTest extends DeduperTest {
    @Test
    public void testBananaRepublicFactoryDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("BananaRepublicFactory.js", "http://bananarepublicfactory.gapfactory.com/browse/product.do?pid=192114001");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("192114001", uniquesAttrs.get(0));
    }
    
    @Test
    public void testBananaRepublicFactoryDeduper2() throws IOException, URISyntaxException {
      final List<String> uniquesAttrs = getUniqueAttributes("BananaRepublicFactory.js", "http://bananarepublicfactory.gapfactory.com/browse/product.do?cid=1045322&vid=1&pid=192114001");
      assertTrue(uniquesAttrs != null);
      assertEquals(1, uniquesAttrs.size());
      assertEquals("192114001", uniquesAttrs.get(0));
    }
      
    @Test
    public void dummyURLCheck() throws IOException, URISyntaxException {
       final List<String> uniquesAttrs = getUniqueAttributes("BananaRepublicFactory.js", "http://www.google.com");
       assertEquals(0, uniquesAttrs.size());
      
  }
    
}

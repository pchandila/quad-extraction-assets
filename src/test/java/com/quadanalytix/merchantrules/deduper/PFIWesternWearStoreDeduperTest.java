package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PFIWesternWearStoreDeduperTest extends DeduperTest {
    @Test
    public void testPFIWesternWearStoreDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PFIWesternWearStore.js", "http://www.pfiwestern.com/ariat-black-bright-lights-cowgirl-boots-79272.html");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("ariat-black-bright-lights-cowgirl-boots-79272", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("PFIWesternWearStore.js", "http://www.pfiwestern.com/wrangler-47mwz-cowboy-cut-original-fit-jeans-48.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("wrangler-47mwz-cowboy-cut-original-fit-jeans-48", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("PFIWesternWearStore.js", "http://www.pfiwestern.com/bootdaddy-moonshine-camo-and-grey-caps-asd23d3er.html");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("bootdaddy-moonshine-camo-and-grey-caps-asd23d3er", uniquesAttrs3.get(0));  
        
    }
}

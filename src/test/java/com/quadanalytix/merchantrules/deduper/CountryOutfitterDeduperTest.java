package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CountryOutfitterDeduperTest extends DeduperTest{
	@Test
    public void testCountryOutfitterDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("CountryOutfitter.js", "http://www.countryoutfitter.com/womens-sky-high-flare-jean/2606603.html?dwvar_2606603_color=Medium%20Blue");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("2606603", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("CountryOutfitter.js", "http://www.countryoutfitter.com/packable-gear-duffel-bag---camoflage/2496191.html?dwvar_2496191_color=Camouflage");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("2496191", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("CountryOutfitter.js", "http://www.countryoutfitter.com/womens-saucy-boots---brown/1140613.html");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("1140613", uniquesAttrs3.get(0));
    }
}

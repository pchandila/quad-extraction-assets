package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PotteryBarnKidsDeduperTest extends DeduperTest {
    @Test
    public void testPotteryBarnKidsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PotteryBarnKids.js", "http://www.potterybarnkids.com/products/art-play-table/?pkey=e%7Cactivity%2Btable%7C2%7Cbest%7C4294960509%7C1%7C24%7C%252Fnew%7C1&cm_src=PRODUCTSEARCH||More%20Options-_-New-_-NoMerchRules");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("art-play-table", uniquesAttrs.get(0));
    }
}

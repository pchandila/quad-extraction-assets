package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class RebeccaMinkoffDeduperTest extends DeduperTest {
    @Test
    public void testRebeccaMinkoffDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/mum-dress?src=catalog");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("mum-dress", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/cady-platform-sandal-resort?src=catalog");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("cady-platform-sandal-resort", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("RebeccaMinkoff.js", "http://www.rebeccaminkoff.com/gia-tank-resort?src=catalog");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("gia-tank-resort", uniquesAttrs2.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ToysRUSDeduperTest extends DeduperTest {
    @Test
    public void testToysRusDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("ToysRUS.js", "http://www.toysrus.com/product/index.jsp?productId=70625306&prodFindSrc=rv");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("70625306", uniquesAttrs.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;


public class RevolveclothingDeduperTest extends DeduperTest{
	@Test
    public void testRevolveclothingDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Revolveclothing.js", "http://www.revolve.com/lpa-pants-57/dp/LPAR-WP1/?d=Womens&page=1&lc=12&itrownum=4&itcurrpage=1&itview=01");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("LPAR-WP1", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Revolveclothing.js", "http://www.revolve.com/raye-tara-pump-in-gold-mirror/dp/RAYE-WZ146/?d=Womens&sectionURL=Direct+Hit");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("RAYE-WZ146", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Revolveclothing.js", "http://www.revolve.com/sol-sana-chloe-boot-in-cognac-suede/dp/SOLS-WZ98/?d=Womens&sectionURL=Direct+Hit");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("SOLS-WZ98", uniquesAttrs3.get(0));
    }
}

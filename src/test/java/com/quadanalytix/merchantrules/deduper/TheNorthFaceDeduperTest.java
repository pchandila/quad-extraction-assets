package com.quadanalytix.merchantrules.deduper;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TheNorthFaceDeduperTest extends DeduperTest {
	@Test
    public void testNorthFaceDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("TheNorthFace.js", "https://www.thenorthface.com/shop/mens-expedition-tights");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("mens-expedition-tights", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("TheNorthFace.js", "https://www.thenorthface.com/shop/shoes-auxiliary-ultra-series/womens-ultra-fastpack-mid-gore-tex?variationId=DXT");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("womens-ultra-fastpack-mid-gore-tex", uniquesAttrs2.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class MarcJacobsDeduperTest extends DeduperTest{
	@Test
    public void testMarcJacobsDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("MarcJacobs.js", "http://www.marcjacobs.com/nicholas-wool-coat/M4005096.html?dwvar_M4005096_color=466&ptype=productpage&viewmode=Ready%20To%20Wear_Outerwear&producttype=Outerwear");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("M4005096", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("MarcJacobs.js", "http://www.marcjacobs.com/dillon-modern-men's-bracelet-44mm/C4001462.html?ptype=productpage&viewmode=Men_Watches&dwvar_C4001462_color=001&producttype=Watches");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("C4001462", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("MarcJacobs.js", "http://www.marcjacobs.com/fergus-bracelet-42mm/C4000102.html");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("C4000102", uniquesAttrs3.get(0));
        
        final List<String> uniquesAttrs4 = getUniqueAttributes("MarcJacobs.js", "http://www.marcjacobs.com/bang-bang-3.4-fl-oz-eau-de-toilette-spray/BANGBANG3.4OZ.html?dwvar_BANGBANG3.4OZ_color=000&ptype=productpage&viewmode=fragrance_#start=3");
        assertTrue(uniquesAttrs4 != null);
        assertEquals(1, uniquesAttrs4.size());
        assertEquals("BANGBANG3.4OZ", uniquesAttrs4.get(0));
    }
}

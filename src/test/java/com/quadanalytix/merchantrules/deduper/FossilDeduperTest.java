package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;


public class FossilDeduperTest extends DeduperTest{
	@Test
    public void testFossilDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Fossil.js", "https://www.fossil.com/us/en/watches/townsman-multifunction-navy-leather-watch-sku-ME1138P.html?rrec=true&recid=product1_rr-me3029p-ME1138P-1417-2");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("ME1138P", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Fossil.js", "https://www.fossil.com/us/en/women/accessories/belts/preston-stripe-belt-sku-bt4330001c.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("bt4330001c", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Fossil.js", "https://www.fossil.com/us/en/connected_accessories/q-grant-chronograph-black-leather-smartwatch-sku-FTW10011P.html?cid=dis:conv:criteo:evergreen:dynamic:neutral:lowerfunnelfb");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("FTW10011P", uniquesAttrs3.get(0));
    }

}

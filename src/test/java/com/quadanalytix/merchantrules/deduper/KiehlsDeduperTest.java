package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class KiehlsDeduperTest extends DeduperTest {
	@Test
    public void testKiehlsDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Kiehls.js", "http://www.kiehls.com/super-multi-corrective-cream/495.html");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("495", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Kiehls.js", "http://www.kiehls.com/close-shavers-squadron-smooth-glider-precision-shave-lotion/KHL975.html?cgid=mens-shave&dwvar_KHL975_size=5.0%20fl.%20oz.%20Bottle#start=5&cgid=mens-shave");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("KHL975", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Kiehls.js", "http://www.kiehls.com/creme-de-corps-whipped-body-butter-limited-edition-mini/407.html?dwvar_407_fragrance=soy%20milk%20and%20honey&dwvar_407_size=2.0%20fl.%20oz.%20Jar&cgid=body-moisturizers");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("407", uniquesAttrs3.get(0));
    }
}

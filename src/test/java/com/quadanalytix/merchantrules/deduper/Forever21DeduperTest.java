package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class Forever21DeduperTest extends DeduperTest {
    @Test
    public void testForever21Deduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Forever21.js", "http://www.forever21.com/Product/Product.aspx?BR=f21&Category=app-main&ProductID=2000191715&VariantID=");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("2000191715", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("Forever21.js", "http://www.forever21.com/Product/Product.aspx?BR=f21&Category=app-main&ProductID=2000054683&VariantID=");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("2000054683", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("Forever21.js", "http://www.forever21.com/Product/Product.aspx?BR=f21&Category=app-main&ProductID=2000213275&VariantID=");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("2000213275", uniquesAttrs2.get(0));
    }
}

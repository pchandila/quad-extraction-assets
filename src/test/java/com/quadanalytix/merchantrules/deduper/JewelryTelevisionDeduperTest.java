package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JewelryTelevisionDeduperTest extends DeduperTest{
	@Test
    public void testJewelryTelevisionDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("JewelryTelevision.js", "http://www.jtv.com/1.15ctw-round-multi-sapphire-and-1.40ctw-round-white-zircon-sterling-silver-j-hoop-earrings/1817673.html");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1817673", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("JewelryTelevision.js", "http://www.jtv.com/disney-mickey-mouse-silvertone-status-mens-watch/1461164.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("1461164", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("JewelryTelevision.js", "http://www.jtv.com/5.00ctw-round-multi-sapphire-sterling-silver-tennis-bracelet/1817660.html");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("1817660", uniquesAttrs3.get(0));
    }

}

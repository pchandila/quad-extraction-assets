package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class HomeDepotDeduperTest extends DeduperTest{
	@Test
    public void testGrouponGoodsDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("HomeDepot.js", "http://www.homedepot.com/p/Home-Decorators-Collection-Montpelier-Black-Velvet-Button-Tufted-Twin-Headboard-860VBLK/202961013");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("Home-Decorators-Collection-Montpelier-Black-Velvet-Button-Tufted-Twin-Headboard-860VBLK", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("HomeDepot.js", "http://www.homedepot.com/p/Lavish-Home-Silver-24-25-in-x-60-in-Memory-Foam-Striped-Extra-Long-Bath-Mat-67-17-P/206408865");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("Lavish-Home-Silver-24-25-in-x-60-in-Memory-Foam-Striped-Extra-Long-Bath-Mat-67-17-P", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("HomeDepot.js", "http://www.homedepot.com/p/Home-Decorators-Collection-Bridgeport-Queen-Size-Bed-in-Antique-White-1872500460/204739562");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("Home-Decorators-Collection-Bridgeport-Queen-Size-Bed-in-Antique-White-1872500460", uniquesAttrs3.get(0));
    }
}

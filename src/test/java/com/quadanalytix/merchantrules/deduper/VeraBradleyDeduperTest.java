package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class VeraBradleyDeduperTest extends DeduperTest {
    @Test
    public void testPotteryBarnDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("VeraBradley.js", "http://www.verabradley.com/product/summer-pajamas/petite-paradise/1004417_205418.uts?N=10031");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1004417", uniquesAttrs.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class PaulasChoiceDeduperTest extends DeduperTest {
    @Test
    public void testPaulasChoiceDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("PaulasChoice.js", "http://www.paulaschoice.com/shop/skin-care-categories/aha-and-bha-exfoliants/_/Clear-Regular-Strength-Anti-Redness-Exfoliating-Solution-With-Two-Percent-Salicylic-Acid");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("Clear-Regular-Strength-Anti-Redness-Exfoliating-Solution-With-Two-Percent-Salicylic-Acid", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("PaulasChoice.js", "http://www.paulaschoice.com/shop/skin-care-categories/body-care/_/Resist-Weightless-Body-Treatment-with-Two-Percent-BHA");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("Resist-Weightless-Body-Treatment-with-Two-Percent-BHA", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("PaulasChoice.js", "http://www.paulaschoice.com/shop/skin-care-categories/aha-and-bha-exfoliants/_/Resist-Daily-Pore-Refining-Treatment-Two-Percent-BHA");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("Resist-Daily-Pore-Refining-Treatment-Two-Percent-BHA", uniquesAttrs2.get(0));
    }
}

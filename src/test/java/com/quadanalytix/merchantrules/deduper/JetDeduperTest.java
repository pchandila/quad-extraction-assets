package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JetDeduperTest extends DeduperTest {
    @Test
    public void testJetDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Jet.js", "https://jet.com/product/Womens-Ali-and-Jay-Colorblock-Maxi-Dress-Size-Large-White/a3066bd9aa1d4ea99919eb048bec5bb0");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("a3066bd9aa1d4ea99919eb048bec5bb0", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Jet.js", "https://jet.com/product/Folgers-Medium-Roast-Ground-Coffee-Classic-48-Oz/b6be792c5c4c4352b556fb119180fa9d");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("b6be792c5c4c4352b556fb119180fa9d", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Jet.js", "https://jet.com/product/Jockey-Womens-Cotton-Stretch-Ankle-Legging/a315ef4352a24701b28effeeb164e5b7");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("a315ef4352a24701b28effeeb164e5b7", uniquesAttrs3.get(0));  
        
    }
}
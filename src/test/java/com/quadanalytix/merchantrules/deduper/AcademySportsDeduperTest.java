package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class AcademySportsDeduperTest extends DeduperTest {
    @Test
    public void testAcademySportsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("AcademySports.js", "http://www.academy.com/shop/pdp/nike-mens-houston-texans-epic-short#repChildCatid=1435731");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1435731", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("AcademySports.js", "http://www.academy.com/shop/pdp/adidas-kids-samba-shoes#repChildCatid=1003559");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1003559", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("AcademySports.js", "http://www.academy.com/shop/pdp/under-armour-mens-vital-woven-pant#repChildCatid=846162");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("846162", uniquesAttrs2.get(0));
    }
}

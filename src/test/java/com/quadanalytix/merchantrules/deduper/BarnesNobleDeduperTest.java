package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BarnesNobleDeduperTest extends DeduperTest{
	@Test
    public void testBarnesNobleDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("BarnesNoble.js", "http://www.barnesandnoble.com/w/harry-potter-and-the-cursed-child-parts-i-ii-j-k-rowling/1123463689?ean=9781338099133");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1123463689", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("BarnesNoble.js", "http://www.barnesandnoble.com/w/people-time-inc/1109625686");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("1109625686", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("BarnesNoble.js", "http://www.barnesandnoble.com/w/color-me-cluttered-durell-h-godfrey/1121798620?ean=9780399183652");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("1121798620", uniquesAttrs3.get(0));
    }
}

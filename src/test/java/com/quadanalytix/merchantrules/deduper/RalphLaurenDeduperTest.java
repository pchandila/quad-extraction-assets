package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class RalphLaurenDeduperTest extends DeduperTest{
	@Test
    public void testRalphLaurenDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("RalphLauren.js", "http://watches.ralphlauren.com/watches/en-us/PDP?collection_id=stirrup&reference_number=RLR0040001&ab=en_US_RLWJ_WATCHESLP_Stirrup_SLOT_2_S1_SHOPNOW");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("RLR0040001", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("RalphLauren.js", "http://www.ralphlauren.com/product/index.jsp?productId=87980726");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("87980726", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("RalphLauren.js", "http://www.ralphlauren.com/product/index.jsp?productId=37973316");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("37973316", uniquesAttrs3.get(0));
    }

}

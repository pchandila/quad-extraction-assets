package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class SaksFifthAvenueDeduperTest extends DeduperTest {
    @Test
    public void testDooneyBurkeDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/main/ProductDetail.jsp?PRODUCT%3C%3Eprd_id=845524446713115&R=603679169254&P_name=2%28X%29IST&N=399546428+1553&FOLDER%3C%3Efolder_id=2534374306418059&bmUID=lykGFW6");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("845524446713115", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/main/ProductDetail.jsp?FOLDER%3C%3Efolder_id=2534374306418061&PRODUCT%3C%3Eprd_id=845524447013671&R=888824338277&P_name=3.1+Phillip+Lim&N=306418061&bmUID=lym118_");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("845524447013671", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("SaksFifthAvenue.js", "http://www.saksfifthavenue.com/main/ProductDetail.jsp?FOLDER%3C%3Efolder_id=2534374306624271&PRODUCT%3C%3Eprd_id=845524447047985&R=887444841006&P_name=Derek+Lam&N=306624271&bmUID=lylZnkO");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("845524447047985", uniquesAttrs3.get(0));
        
    }
}

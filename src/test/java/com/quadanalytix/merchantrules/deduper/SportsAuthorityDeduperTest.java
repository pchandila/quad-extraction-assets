package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class SportsAuthorityDeduperTest extends DeduperTest {

    @Test
    public void testSportsAuthorityDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("SportsAuthority.js", "http://www.sportsauthority.com/NIKE-Adult-Hyperbeast-2-0-Lineman-Gloves/product.jsp?productId=16480366&cp=2360707&parentPage=family");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("16480366", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("SportsAuthority.js", "http://www.sportsauthority.com/Nike-Mens-Mercurial-Victory-V-FG-CR7-Low-Soccer-Cleats/product.jsp?productId=66277646&cp=3077570.61842836.62190866.4448145&parentPage=family");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("66277646", uniquesAttrs.get(0));
    }
    
}

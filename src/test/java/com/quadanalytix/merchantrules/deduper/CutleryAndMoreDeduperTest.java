package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CutleryAndMoreDeduperTest extends DeduperTest {
    @Test
    public void testCutleryAndMoreDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("CutleryandMore.js", "http://www.cutleryandmore.com/zojirushi/home-bakery-virtuoso-bread-machine-p125044");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("zojirushi", uniquesAttrs.get(0));
        assertEquals("p125044", uniquesAttrs.get(1));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.Test;

public class NikeDeduperTest extends DeduperTest{
	@Test
    public void testNikeDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Nike.js", "http://store.nike.com/us/en_us/pd/aeroloft-womens-running-vest/pid-11192346/pgid-11192340");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("11192340", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Nike.js", "http://store.nike.com/us/en_us/pd/brasilia-6-extra-small-duffel-bag/pid-10823353/pgid-11184095");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("11184095", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Nike.js", "http://store.nike.com/us/en_us/pd/pro-hyperstrong-2-ankle-sleeve/pid-11189193/pgid-11189194");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("11189194", uniquesAttrs3.get(0));
    }
}

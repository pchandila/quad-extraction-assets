package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class OnlineshoescomDeduperTest extends DeduperTest {
    @Test
    public void testOnlineshoescomDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Onlineshoescom.js", "http://www.onlineshoes.com/mens-feetures-elitemax-cushion-no-show-tab-1-pk-berry-lava-p_id501389");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("mens-feetures-elitemax-cushion-no-show-tab-1-pk-berry-lava-p_id501389", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Onlineshoescom.js", "http://www.onlineshoes.com/womens-dansko-tamara-black-burnished-full-grain-p_id304086");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("womens-dansko-tamara-black-burnished-full-grain-p_id304086", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Onlineshoescom.js", "http://www.onlineshoes.com/mens-sof-sole-leather-lube-clear-p_id349826");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("mens-sof-sole-leather-lube-clear-p_id349826", uniquesAttrs3.get(0));
        
    }
}

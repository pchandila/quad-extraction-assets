package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class AriatDeduperTest extends DeduperTest {
    @Test
    public void testAriatDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Ariat.js", "http://www.ariat.com/ROSE_W_ACC.html?dwvar_ROSE__W__ACC_color=CHARCOAL_HEATHER#start=1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("ROSE_W_ACC", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Ariat.js", "http://www.ariat.com/SHOW_GARMENT_BAG_W_M_ACC.html?dwvar_SHOW__GARMENT__BAG__W__M__ACC_color=BLACK_TAN#start=1");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("SHOW_GARMENT_BAG_W_M_ACC", uniquesAttrs2.get(0));
        
    }
}

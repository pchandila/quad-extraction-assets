package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JCrewDeduperTest extends DeduperTest {
    @Test
    public void testJCrewDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("JCrew.js", "https://www.jcrew.com/in/boys_category/teesandpolos/polos/PRDOVR~C9407/C9407.jsp");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("C9407", uniquesAttrs.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class NeimanMarcusDeduperTest extends DeduperTest{
	@Test
    public void testNeimanMarcusDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("NeimanMarcus.js", "http://www.neimanmarcus.com/Shop-All-Bedding/prod155380018_cat54620732__/p.prod?icid=&searchType=EndecaDrivenCat&rte=%252Fcategory.jsp%253FitemId%253Dcat54620732%2526pageSize%253D30%2526Nao%253D0%2526refinements%253D&eItemId=prod155380018&cmCat=product");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("prod155380018_cat54620732__", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("NeimanMarcus.js", "http://www.neimanmarcus.com/Dian-Austin-Couture-Home-Olivia-Curtain-108-L/prod106470001/p.prod");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("prod106470001", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("NeimanMarcus.js", "http://www.neimanmarcus.com/Burberry-London-Rabbit-Fur-Collar-Military-Coat-Olive/prod180500322/p.prod?parentId=cat16730740&eItemId=prod180500322&rte=%2Fcategory.jsp%3FitemId%3Dcat16730740%26pageSize%3D30%26Nao%3D0%26refinements%3D&searchType=EndecaDrivenCat&cmCat=product&icid=");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("prod180500322", uniquesAttrs3.get(0));
    }
}

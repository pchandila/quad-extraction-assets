package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class WestElmDeduperTest extends DeduperTest {
    @Test
    public void testWilliamsSonomaDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("WestElm.js", "http://www.westelm.com/products/souk-rug-b865/?pkey=cpattern-rugs-flooring%7C%7C&cm_src=pattern-rugs-flooring||NoFacet-_-NoFacet-_--_-");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("souk-rug-b865", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("WestElm.js", "http://www.westelm.com/products/notre-monde-tray-d3091/?cm_src=tabletop-kitchen-stock-sale||NoFacet-_-NoFacet-_--_-");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("notre-monde-tray-d3091", uniquesAttrs.get(0));
        
    }
    
}

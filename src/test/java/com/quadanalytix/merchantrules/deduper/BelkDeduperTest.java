package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BelkDeduperTest extends DeduperTest{
	@Test
    public void testBelkDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/PRD~99999998056MADI/Madison+Madison+Classic+Fit+Light+Grey+Shark+Suit+Separates.jsp?navPath=Men/Shop/SuitsSuitSeparates&ZZ%3C%3EtP=4294921399&fO=Category_Path%3A%2FBelk_Primary%2FMen%2FSuitsSuitSeparates&ZZ_OPT=Y&FOLDER%3C%3Efolder_id=2534374302215377&bmUID=l10OUrO&changeViewInd=y");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("PRD~99999998056MADI", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/PRD~2600883CSW245/Cappelli+Straworld+Toyo+Sun+Hat.jsp?navPath=Handbags_And_Accessories/Shop/Accessories/Hats&ZZ%3C%3EtP=4294922314&fO=Category_Path%3A%2FBelk_Primary%2FHandbags_And_Accessories%2FAccessories&ZZ_OPT=Y&PRODUCT%3C%3Eprd_id=845524441970185&FOLDER%3C%3Efolder_id=2534374302217879&bmUID=lhc2ueY&changeViewInd=y");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("PRD~2600883CSW245", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Belk.js", "http://www.belk.com/AST/Main/Belk_Primary/PRD~9200324SWEPT948068/Waverly+Swept+Away+Bedding+Collection.jsp?navPath=BedBath/Shop/Bed/BeddingCollections&ZZ%3C%3EtP=4294601145&fO=Category_Path%3A%2FBelk_Primary%2FBedBath%2FBed&ZZ_OPT=Y&FOLDER%3C%3Efolder_id=2534374302460324&bmUID=lhbWx0p&changeViewInd=y");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("PRD~9200324SWEPT948068", uniquesAttrs3.get(0));
    }
}

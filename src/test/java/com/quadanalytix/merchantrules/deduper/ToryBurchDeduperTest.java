package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ToryBurchDeduperTest extends DeduperTest{
	@Test
    public void testToryBurchDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("ToryBurch.js", "http://www.toryburch.com/wool-sweater/52153645.html?cgid=clothing-newarrivals&start=1&dwvar_52153645_color=257");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("52153645", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("ToryBurch.js", "http://www.toryburch.com/dream-catcher-pendant-necklace/51155762.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("51155762", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("ToryBurch.js", "http://www.toryburch.com/solana-espadrille-sandal/51158602.html?cgid=shoes-heels&start=7&dwvar_51158602_color=253");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("51158602", uniquesAttrs3.get(0));
    }
}

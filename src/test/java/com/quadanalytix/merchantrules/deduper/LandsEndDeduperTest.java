package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class LandsEndDeduperTest extends DeduperTest {
    @Test
    public void testLandsEndDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("LandsEnd.js", "http://www.landsend.com/products/medium-open-top-natural-canvas-tote-bag/id_191995?sku_0=::AEZ");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("191995", uniquesAttrs.get(0));
    }
}

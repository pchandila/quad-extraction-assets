package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CliniqueDeduperTest extends DeduperTest {
	@Test
    public void testCliniqueDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Clinique.js", "http://www.clinique.com/product/1599/9285/makeup/foundations/acne-solutions-liquid-makeup");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("acne-solutions-liquid-makeup", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Clinique.js", "http://www.clinique.com/product/1668/34750/skin-care/acne/acne-solutions-cleansing-gel");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("acne-solutions-cleansing-gel", uniquesAttrs2.get(0));
    }

}

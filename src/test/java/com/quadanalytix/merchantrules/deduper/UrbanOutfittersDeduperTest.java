package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class UrbanOutfittersDeduperTest extends DeduperTest {
    @Test
    public void testUrbanOutfittersDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("UrbanOutfitters.js", "http://www.urbanoutfitters.com/urban/catalog/productdetail.jsp?id=39670773&category=M_APP_POLOS");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("39670773", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("UrbanOutfitters.js", "http://www.urbanoutfitters.com/urban/catalog/productdetail.jsp?id=39668256&category=M_APP_POLOS");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("39668256", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("UrbanOutfitters.js", "http://www.urbanoutfitters.com/urban/catalog/productdetail.jsp?id=40577041&category=W-NEW-DRESSES");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("40577041", uniquesAttrs2.get(0));
    }
}

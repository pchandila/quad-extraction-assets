package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class UltaDeduperTest extends DeduperTest {
    @Test
    public void testUltaDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Ulta.js", "http://www.ulta.com/original-foundation-broad-spectrum-spf-15?productId=VP11362");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("VP11362", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("Ulta.js", "http://www.ulta.com/pro-45-starter-kit?productId=xlsImpprod3950155");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("xlsImpprod3950155", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("Ulta.js", "http://www.ulta.com/multiuse-skincare-oil?productId=xlsImpprod1800041");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("xlsImpprod1800041", uniquesAttrs2.get(0));
    }
}

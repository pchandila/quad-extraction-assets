package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class FootLockerDeduperTest extends DeduperTest {
    @Test
    public void testFootLockerDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("FootLocker.js", "http://www.footlocker.com/product/model:61710/sku:1405/converse-all-star-leather-hi-mens/all-black/black/");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("61710", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("FootLocker.js", "http://www.footlocker.com/product/model:256441/sku:18837091/nike-legend-2.0-long-sleeve-t-shirt-mens/grey/grey/");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("256441", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("FootLocker.js", "http://www.footlocker.com/product/model:236830/sku:AQ5936/adidas-ultra-boost-womens/grey/pink/");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("236830", uniquesAttrs2.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class OverStockDeduperTest extends DeduperTest {
    @Test
    public void testOverStockDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Overstock.js", "http://www.overstock.com/Home-Garden/Breville-BOV800XL-Toaster-Oven/4317949/product.html?searchidx=0");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("Breville-BOV800XL-Toaster-Oven", uniquesAttrs.get(0));
        assertEquals("4317949", uniquesAttrs.get(1));
    }
}

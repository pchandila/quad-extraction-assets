package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class GapFactoryDeduperTest extends DeduperTest {
    @Test
    public void GapFactoryDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("GapFactory.js", "http://www.gapfactory.com/browse/product.do?cid=1049456&vid=1&pid=451203021");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("451203021", uniquesAttrs.get(0));
    }
    
    @Test
    public void testBananaRepublicFactoryDeduper2() throws IOException, URISyntaxException {
      final List<String> uniquesAttrs = getUniqueAttributes("GapFactory.js", "http://www.gapfactory.com/browse/product.do?cid=1049456&vid=1&pid=190725011");
      assertTrue(uniquesAttrs != null);
      assertEquals(1, uniquesAttrs.size());
      assertEquals("190725011", uniquesAttrs.get(0));
    }
      
    @Test
    public void dummyURLCheck() throws IOException, URISyntaxException {
       final List<String> uniquesAttrs = getUniqueAttributes("GapFactory.js", "http://www.google.com");
       assertEquals(0, uniquesAttrs.size());
      
  }
    
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TillysDeduperTest extends DeduperTest{
	@Test
    public void testTillysDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Tillys.js", "https://www.tillys.com/product/Wendylou/Decor/Spiky-Succulent-Plant/Green-Combo/289411549");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("Spiky-Succulent-Plant", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Tillys.js", "https://www.tillys.com/product/Destined/Nails/DESTINED-Nail-Color/Call-Me-Captain/273976210");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("DESTINED-Nail-Color", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Tillys.js", "https://www.tillys.com/product/K-WAY/Jackets/K-WAY-Le-Vrai-Claude-3-0-Mens-Windbreaker/Gold/295933442");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("K-WAY-Le-Vrai-Claude-3-0-Mens-Windbreaker", uniquesAttrs3.get(0));
    }
}

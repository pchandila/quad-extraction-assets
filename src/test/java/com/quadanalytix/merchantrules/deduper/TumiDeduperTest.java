package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TumiDeduperTest extends DeduperTest {
    @Test
    public void testTumiDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Tumi.js", "http://www.tumi.com/p/tumi-two-piece-case-for-iphone-6-and-6s-0114207DL2GM");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("0114207DL2GM", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs1 = getUniqueAttributes("Tumi.js", "http://www.tumi.com/p/archer-backpack-063012B");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("063012B", uniquesAttrs1.get(0));
        

        final List<String> uniquesAttrs2 = getUniqueAttributes("Tumi.js", "http://www.tumi.com/p/andersen-slim-commuter-brief-0222640AT2");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("0222640AT2", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Tumi.js", "http://www.tumi.com/p/classic-travel-blazer-0S67404GRY38");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("0S67404GRY38", uniquesAttrs3.get(0));
    }
}
package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class QVCDeduperTest extends DeduperTest {
    @Test
    public void testQVCDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("QVC.js", "http://www.qvc.com/NFL-Suede-Jacket-with-Snap-Cover-Zipper-Closure.product.A255719.html?sc=A255719-User&cm_sp=VIEWPOSITION-_-5-_-A255719&catentryImage=http://images.qvc.com/is/image/a/19/a255719.001?$uslarge$");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("A255719", uniquesAttrs.get(0));
    }
}

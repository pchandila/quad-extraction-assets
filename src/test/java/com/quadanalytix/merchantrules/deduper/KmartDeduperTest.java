package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class KmartDeduperTest extends DeduperTest {
    @Test
    public void testKmartDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Kmart.js", "http://www.kmart.com/jaclyn-smith-women-s-shirred-v-neck-top-abstract/p-027VA78266412P?prdNo=1&blockNo=1&blockType=G1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("p-027VA78266412P", uniquesAttrs.get(0));
    }
}

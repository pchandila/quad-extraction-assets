package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DSWDeduperTest extends DeduperTest {
    @Test
    public void testDSWDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("DSW.js", "http://www.dsw.com/shoe/fergalicious+graceful+gladiator+sandal?prodId=346632");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("346632", uniquesAttrs.get(0));
    }
}

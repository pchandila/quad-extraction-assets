package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class BootBarnDeduperTest extends DeduperTest{
	@Test
    public void testBootBarnDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("BootBarn.js", "http://www.bootbarn.com/Rocky-Men's-TMC-Postal-Approved-Duty-Shoes/2010773,default,pd.html?dwvar_2010773_color=Black");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("2010773", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("BootBarn.js", "http://www.bootbarn.com/Corral-Women's-Cross-and-Wing-Sequin-Inlay-Square-Toe-Western-Boots/2009470,default,pd.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("2009470", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("BootBarn.js", "http://www.bootbarn.com/Double-H-Men's-Work-Western-12%22-Work-Boots/1003121,default,pd.html?dwvar_1003121_color=Medium%20Brown");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("1003121", uniquesAttrs3.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ShoescomDeduperTest extends DeduperTest {
    @Test
    public void testShoescomDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Shoescom.js", "http://www.shoes.com/mens-lacoste-jouer-slip-on-316-1-grey-p2_id475229");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("mens-lacoste-jouer-slip-on-316-1-grey-p2_id475229", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Shoescom.js", "http://www.shoes.com/mens-new-balance-crew-neck-sweatshirt-athletic-grey-p2_id392350");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("mens-new-balance-crew-neck-sweatshirt-athletic-grey-p2_id392350", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Shoescom.js", "http://www.shoes.com/womens-hobo-sheila-mahogany-p2_id495841");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("womens-hobo-sheila-mahogany-p2_id495841", uniquesAttrs3.get(0));
        
    }
}

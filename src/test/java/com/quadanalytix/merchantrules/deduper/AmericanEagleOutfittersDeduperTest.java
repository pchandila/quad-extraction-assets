package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class AmericanEagleOutfittersDeduperTest extends DeduperTest{
	@Test
    public void testAmericanEagleOutfittersDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("AmericanEagleOutfitters.js", "https://www.ae.com/women-aeo-ahh-mazingly-soft-boyfriend-shirt-white/web/s-prod/1354_7201_100?cm=sIN-cINR&catId=cat90038");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1354_7201_100", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("AmericanEagleOutfitters.js", "https://www.ae.com/aerie-body-10-oz-lotion-be-cozy/aerie/s-prod/0822_0109_635?cm=sUS-cUSD&catId=cat7670086");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("0822_0109_635", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("AmericanEagleOutfitters.js", "https://www.ae.com/women-aeo-apt-twin-xl-quilt-set-multi/web/s-prod/0573_1211_900?cm=sUS-cUSD&catId=cat7980006");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("0573_1211_900", uniquesAttrs3.get(0));
    }
}

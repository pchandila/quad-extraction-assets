package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CasaDeduperTest extends DeduperTest {
    @Test
    public void testCasaDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Casa.js", "http://www.casa.com/p/cuisinart-replacement-carafes-229308");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("229308", uniquesAttrs.get(0));
    }
}

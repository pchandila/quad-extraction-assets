package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DoverSaddleryDeduperTest extends DeduperTest {
    @Test
    public void testDoverSaddleryDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("DoverSaddlery.js", "http://www.doversaddlery.com/hrsmans-pride-railrazer-set-of-4/p/X1-2755/");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("X1-2755", uniquesAttrs.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DicksSportingGoodsDeduperTest extends DeduperTest {
    @Test
    public void testDicksSportingGoodsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("DicksSportingGoods.js", "http://www.dickssportinggoods.com/product/index.jsp?productId=57808686&ab=TopNav_TeamSports_Football_IntegratedApparel&cp=4413887.4414019&categoryId=11757750");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("57808686", uniquesAttrs.get(0));
    }
}

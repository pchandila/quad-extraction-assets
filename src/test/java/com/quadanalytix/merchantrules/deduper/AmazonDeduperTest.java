package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class AmazonDeduperTest extends DeduperTest {
    @Test
    public void testAmazonDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Amazon.js", "http://www.amazon.com/OluKai-Kaiulani-Boot-Womens-Black/dp/B00H58VQ7G/ref=sr_1_104?s=apparel&ie=UTF8&qid=1455736428&sr=1-104&nodeID=679337011&refinements=p_89%3AOluKai");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("B00H58VQ7G", uniquesAttrs.get(0));
    }
}

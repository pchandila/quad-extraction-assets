package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DillardsDeduperTest extends DeduperTest {
    @Test
    public void testDillardsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Dillards.js", "http://www.dillards.com/p/adrianna-papell-short-sleeve-sequined-long-skirt-gown/503614499?di=03910084_zi_champagne&categoryId=894&facetCache=pageSize%3D100%26beginIndex%3D0%26orderBy%3D1");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("503614499", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("Dillards.js", "http://www.dillards.com/p/preston--york-felicia-short-sleeve-lace-sheath-dress/505009646?di=04412577_zi_cruise_blue&categoryId=894&facetCache=pageSize%3D100%26beginIndex%3D0%26orderBy%3D1");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("505009646", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("Dillards.js", "http://www.dillards.com/p/adrianna-papell-beaded-short-sleeve-gown/505096382?di=04437454_zi_bluemist&categoryId=894&facetCache=pageSize%3D100%26beginIndex%3D0%26orderBy%3D1");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("505096382", uniquesAttrs2.get(0));
    }
}

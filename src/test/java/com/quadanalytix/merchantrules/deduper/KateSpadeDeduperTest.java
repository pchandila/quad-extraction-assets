package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class KateSpadeDeduperTest extends DeduperTest {
    @Test
    public void testKateSpadeDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("KateSpade.js", "https://www.katespade.com/products/francy-sandals/S030014SY.html?dwvar_S030014SY_color=001&cgid=ks-shoes-sandals#start=4&cgid=ks-shoes-sandals");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("S030014SY", uniquesAttrs.get(0));
    }
}

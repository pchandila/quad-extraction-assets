package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DooneyBourkeDeduperTest extends DeduperTest {
    @Test
    public void testDooneyBurkeDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("DooneyBourke.js", "http://www.dooney.com/cleo-collection-small-leisure-shopper/BCLEO2352.html?dwvar_BCLEO2352_color=EVGEPATN#q=cleo+collection&start=2&cgid=dooney");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("BCLEO2352", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("DooneyBourke.js", "http://www.dooney.com/claremont-drawstring/LF038.html?dwvar_LF038_color=LFBLPABS#q=claremont+drawstring&start=1&cgid=dooney-collections-claremont");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("LF038", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("DooneyBourke.js", "http://www.dooney.com/carley-kimberly/EN310.html?cgid=dooney-bags&dwvar_EN310_color=EVBMSVNA#srule=collection-name-sort&sz=12&start=58&cgid=dooney-bags");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("EN310", uniquesAttrs3.get(0));
        
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JCPenneyDeduperTest extends DeduperTest {
    @Test
    public void testJCPenneyDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("JCPenney.js", "http://www.jcpenney.com/for-the-home/lighting-lamps/design-by-conran-brooke-collection/prod.jump?ppId=ens6002820030&catId=cat1002460010&subcatId=cat100240019&deptId=dept20000011&&_dyncharset=UTF-8&colorizedImg=DP1209201317012033M.tif");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("ens6002820030", uniquesAttrs.get(0));
    }
}

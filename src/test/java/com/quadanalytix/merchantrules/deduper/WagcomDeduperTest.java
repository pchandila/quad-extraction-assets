package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import org.junit.Test;

public class WagcomDeduperTest extends DeduperTest {
	@Test
    public void testWagDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Wagcom.js", "http://www.wag.com/dog/p/barklogic-tick-flea-prevention-and-treatment-spray-448976?qid=2717129885&sr=1-4&sku=UCT-004");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("448976", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Wagcom.js", "http://www.wag.com/dog/p/frontline-flea-tick-spray-8-5-oz-452519?qid=3273514678&sr=1-3&sku=FPS-30653");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("452519", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Wagcom.js", "http://www.wag.com/cat/p/van-ness-non-tip-dish-with-rubber-ring-16-oz-1113059?qid=3810140384&sr=1-2&sku=VNS-055");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("1113059", uniquesAttrs3.get(0));
    }
}

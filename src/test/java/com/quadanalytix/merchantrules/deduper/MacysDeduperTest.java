package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class MacysDeduperTest extends DeduperTest {
    @Test
    public void testMacysDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Macys.js", "http://www1.macys.com/shop/product/dustin-ii-fabric-living-room-chair-38w-x-35d-x-28h?ID=851201&CategoryID=36166#fn=sp%3D1%26spc%3D350%26ruleId%3D54%26slotId%3Drec(2)");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("851201", uniquesAttrs.get(0));
    }
}

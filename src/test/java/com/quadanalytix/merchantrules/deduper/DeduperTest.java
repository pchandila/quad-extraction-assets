package com.quadanalytix.merchantrules.deduper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.mozilla.javascript.NativeArray;

import com.quadanalytix.extractionservice.jsfunctions.JSFunction;

public class DeduperTest {
    List<String> getUniqueAttributes(final String fileName, final String url) throws IOException, URISyntaxException {
        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream("dedupers/" + fileName)) {
            final String script = IOUtils.toString(input, "UTF-8");
            final JSFunction deduper = new JSFunction(Arrays.asList("url"), script);
            final Object value = deduper.apply(new Object[] { url });
            return getJavaList((NativeArray) value);    
        }
    }

    /**
     * Converts native array to java list.
     * @param value
     * @return java list
     */
    List<String> getJavaList(final NativeArray array) {
        final List<String> uniqueAttributes = new ArrayList<>();
        for (Object o : array.getIds()) {
            final int index = (Integer) o;
            uniqueAttributes.add((String) array.get(index, null));
        }
        return uniqueAttributes;
    }
}
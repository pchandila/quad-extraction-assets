package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class MichaelKorsDeduperTest extends DeduperTest {
    @Test
    public void testMichaelKorsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("MichaelKors.js", "http://www.michaelkors.com/fox-fur-cuffed-wool-melton-princess-coat/_/R-US_511PKF524?No=5&color=0634#");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("511PKF524", uniquesAttrs.get(0));
    }
}

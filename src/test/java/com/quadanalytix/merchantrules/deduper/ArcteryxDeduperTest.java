package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ArcteryxDeduperTest extends DeduperTest {
	@Test
    public void testArcteryxDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Arcteryx.js", "http://arcteryx.com/product.aspx?language=EN&gender=mens&category=Shirts_and_Tops&model=Gryson-Shirt-LS");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("Gryson-Shirt-LS", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Arcteryx.js", "http://arcteryx.com/product.aspx?language=EN&gender=mens&category=Footwear&subcat=Liners&model=GORE-TEX-Insulated-Mid-Liner");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("GORE-TEX-Insulated-Mid-Liner", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Arcteryx.js", "http://arcteryx.com/product.aspx?language=EN&gender=mens&category=Footwear&subcat=Boots&model=Bora2-Mid-GTX-Hiking-Boot");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("Bora2-Mid-GTX-Hiking-Boot", uniquesAttrs3.get(0));
    }
}

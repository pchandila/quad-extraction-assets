package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CostcoDeduperTest extends DeduperTest {
    @Test
    public void testCostcoDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Costco.js", "http://www.costco.com/-Zadro-7%22-Cordless-Dual-Sided-LED-Lighted-Vanity-Mirror.product.100107698.html");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("100107698", uniquesAttrs.get(0));
    }
}

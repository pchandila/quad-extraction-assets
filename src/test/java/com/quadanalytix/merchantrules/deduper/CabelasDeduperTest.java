package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CabelasDeduperTest extends DeduperTest{
	@Test
    public void testCabelasDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Cabelas.js", "http://www.cabelas.com/catalog/product.jsp?productId=1546488");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("1546488", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Cabelas.js", "http://www.cabelas.com/product/lowrance-reg-hds-7-gen2-touch-sonar-gps-combo-with-hdi-tansducer/2130797.uts");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("2130797", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Cabelas.js", "http://www.cabelas.com/product/camping/kids-camping%7C/pc/104795280/c/104413680/strider-174-sport-balance-starter-bike/2034299.uts?destination=%2Fcatalog%2Fbrowse%2F_%2FN-1116073");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("2034299", uniquesAttrs3.get(0));
    }
	
}

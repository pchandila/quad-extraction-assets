package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class KohlsDeduperTest extends DeduperTest {
    @Test
    public void testKohlsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Kohls.js", "http://www.kohls.com/product/prd-495025/levis-529-curvy-bootcut-jeans-womens.jsp");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("prd-495025", uniquesAttrs.get(0));
    }
}

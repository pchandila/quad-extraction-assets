package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class RestorationHardwareDeduperTest extends DeduperTest {
    @Test
    public void testRestorationHardwareDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("RestorationHardware.js", "http://www.restorationhardware.com/catalog/product/product.jsp?productId=prod2111674&categoryId=cat2110006");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("2111674", uniquesAttrs.get(0));
    }
}

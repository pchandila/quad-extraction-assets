package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CavendersDeduperTest extends DeduperTest{

	@Test
    public void testCavendersDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Cavenders.js", "https://www.cavenders.com/western/women/womens-boots-shoes/boot-accessories/insoles/AR008003");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("AR008003", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Cavenders.js", "https://www.cavenders.com/western/accessories/western-belts/mens-basic-western-belts/9752444");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("9752444", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Cavenders.js", "https://www.cavenders.com/western/cowboy-hats/hat-care-accessories/hat-care-accessories/PRESHATBOOT");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("PRESHATBOOT", uniquesAttrs3.get(0));
    }
}

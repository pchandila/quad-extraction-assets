package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class NordstromDeduperTest extends DeduperTest {
    @Test
    public void testNordstromDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Nordstrom.js", "http://shop.nordstrom.com/s/bobeau-asymmetric-knit-maxi-skirt-regular-petite/3184439?origin=category-personalizedsort&contextualcategoryid=0&fashionColor=Piper+Red&resultback=0");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("3184439", uniquesAttrs.get(0));
    }
}

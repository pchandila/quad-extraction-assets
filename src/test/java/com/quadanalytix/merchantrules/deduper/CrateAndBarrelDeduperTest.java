package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CrateAndBarrelDeduperTest extends DeduperTest {
    @Test
    public void testCrateAndBarrelDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("CrateandBarrel.js", "http://www.crateandbarrel.com/moccamaster-10-cup-coffee-maker/s292552");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("s292552", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("CrateandBarrel.js", "http://www.crateandbarrel.com/kitchen-and-food/specialty-cookware/le-creuset-ink-everyday-pan/f47106");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("f47106", uniquesAttrs.get(0));
    }
}

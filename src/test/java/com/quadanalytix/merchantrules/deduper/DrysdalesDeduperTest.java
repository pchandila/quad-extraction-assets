package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class DrysdalesDeduperTest extends DeduperTest {
    @Test
    public void testDrysdalesDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Drysdales.js", "http://www.drysdales.com/tru-cal-men-s-camo-swim-trunk.html");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("tru-cal-men-s-camo-swim-trunk", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("Drysdales.js", "http://www.drysdales.com/catalog/product/view/id/1693529/s/tin-haul-men-s-11-quot---quot-triangles-quot--anvil-of-power-brown-patchwork-western-boot/category/4196/");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("tin-haul-men-s-11-quot---quot-triangles-quot--anvil-of-power-brown-patchwork-western-boot", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("Drysdales.js", "http://www.drysdales.com/southern-grace-multi-stone-necklace-with-brown-tassel-and-silver-feather.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("southern-grace-multi-stone-necklace-with-brown-tassel-and-silver-feather", uniquesAttrs2.get(0));
    }
}

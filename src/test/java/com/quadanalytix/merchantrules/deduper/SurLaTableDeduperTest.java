package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class SurLaTableDeduperTest extends DeduperTest {
    @Test
    public void testSurLaTableDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("SurLaTable.js", "http://www.surlatable.com/product/PRO-1017482/Cuisinart-Grind-and-Brew-Coffee-Maker");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("PRO-1017482", uniquesAttrs.get(0));
        assertEquals("Cuisinart-Grind-and-Brew-Coffee-Maker", uniquesAttrs.get(1));
        
        uniquesAttrs = getUniqueAttributes("SurLaTable.js", "http://www.surlatable.com/product/PRO-1225762/Technivorm+Moccamaster+Coffee+Maker+with+Thermal+Carafe+Polished+Silver;jsessionid=47D74811DA3A8185CE00BAEC7E199BD2.slt-app-01-p-app2");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("PRO-1225762", uniquesAttrs.get(0));
        assertEquals("Technivorm+Moccamaster+Coffee+Maker+with+Thermal+Carafe+Polished+Silver", uniquesAttrs.get(1));
        
        uniquesAttrs = getUniqueAttributes("SurLaTable.js", "http://www.surlatable.com/product/PRO-1225762/Technivorm+Moccamaster+Coffee+Maker+with+Thermal+Carafe+Polished+Silver;jsessionid=70226DD34101A3A355344655C255583A.slt-app-01-p-app2");
        assertTrue(uniquesAttrs != null);
        assertEquals(2, uniquesAttrs.size());
        assertEquals("PRO-1225762", uniquesAttrs.get(0));
        assertEquals("Technivorm+Moccamaster+Coffee+Maker+with+Thermal+Carafe+Polished+Silver", uniquesAttrs.get(1));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ZapposDeduperTest extends DeduperTest {
    @Test
    public void testZapposDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Zappos.js", "http://www.zappos.com/crocs-a-leigh-brushed-metallic-flip-sliver");
        final List<String> uniquesAttrs1 = getUniqueAttributes("Zappos.js", "http://www.zappos.com/p/laredo-crocoloco-cognac-croc-print/product/8543471/color/564596");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("crocs-a-leigh-brushed-metallic-flip-sliver", uniquesAttrs.get(0));
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("8543471", uniquesAttrs1.get(0));
    }
}

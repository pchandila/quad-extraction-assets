package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class LululemonDeduperTest extends DeduperTest {
    @Test
    public void testLululemonDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Lululemon.js", "http://shop.lululemon.com/p/men-mats-props/Mens-The-Towel/_/prod1370416?rcnt=6&N=8ax&cnt=10&color=LU9531S_4411");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("prod1370416", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs1 = getUniqueAttributes("Lululemon.js", "http://shop.lululemon.com/p/men-mats-props/M-The-Reversible-Un-Mat/_/prod6750222?rcnt=5&N=8ax&cnt=10&color=LU9A76S_4780");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("prod6750222", uniquesAttrs1.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Lululemon.js", "http://shop.lululemon.com/p/jackets-and-hoodies-jackets/Define-Jacket-SE-Exhale/_/prod2040105?rcnt=1&N=8a1&cnt=12&color=LW4ID2S_020028");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("prod2040105", uniquesAttrs2.get(0));
    }
}

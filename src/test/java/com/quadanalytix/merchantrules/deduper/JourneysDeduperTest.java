package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class JourneysDeduperTest extends DeduperTest {
    @Test
    public void testKmartDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Journeys.js", "http://www.journeys.com/kidz/product.aspx?id=377475&c=1961&g=m");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("377475", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Journeys.js", "http://www.journeys.com/kidz/product.aspx?id=381635&ag=i&c=901&s=EACH&g=m&p=1");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("381635", uniquesAttrs2.get(0));
    }
}

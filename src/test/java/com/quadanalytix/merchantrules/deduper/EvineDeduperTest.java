package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class EvineDeduperTest extends DeduperTest {
	@Test
    public void testEvineDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("EVINE.js", "http://www.evine.com/Product/725-067");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("725-067", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("EVINE.js", "http://www.evine.com/Product/724-785");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("724-785", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("EVINE.js", "http://www.evine.com/Product/723-294");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("723-294", uniquesAttrs3.get(0));
    }
}

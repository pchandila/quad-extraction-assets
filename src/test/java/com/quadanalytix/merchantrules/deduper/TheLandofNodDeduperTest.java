package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class TheLandofNodDeduperTest extends DeduperTest {
    @Test
    public void testTheLandofNodDeduper() throws IOException, URISyntaxException {
        List<String> uniquesAttrs = getUniqueAttributes("TheLandofNod.js", "http://www.landofnod.com/shape-shifter-wall-shelf/f16939");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("f16939", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("TheLandofNod.js", "http://www.landofnod.com/straight-and-narrow-wall-rack-white/f7885");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("f7885", uniquesAttrs.get(0));
        
        uniquesAttrs = getUniqueAttributes("TheLandofNod.js", "http://www.landofnod.com/little-study-drawers/s196606?rv=Storage%3ashelves%3aStraight+%26+Narrow+Wall+Rack+%28White%29+Family+Page");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("s196606", uniquesAttrs.get(0));
    }
}
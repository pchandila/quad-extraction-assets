package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class ScheelsDeduperTest extends DeduperTest {
    @Test
    public void testScheelsDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("Scheels.js", "http://www.scheels.com/shop/en/scheels-catalog/footwear/mens-casual-shoes/mens-born-cabot-sandals-732211-h08923");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("732211-h08923", uniquesAttrs.get(0));
    }
}

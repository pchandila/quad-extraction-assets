package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;


public class ClarinsDeduperTest extends DeduperTest{
	@Test
    public void testClarinsDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("Clarins.js", "http://www.clarinsusa.com/en/sunscreen-for-face-wrinkle-control-cream-spf-30/0140119.html#prefn1=concern&prefv1=High+Protection&start=1");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("0140119", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("Clarins.js", "http://www.clarinsusa.com/en/joli-rouge-lipstick/0443491.html");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("0443491", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("Clarins.js", "http://www.clarinsusa.com/en/eyebrow-pencil/0421341.html#pmax=50&pmin=0&start=1");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("0421341", uniquesAttrs3.get(0));
    }
}

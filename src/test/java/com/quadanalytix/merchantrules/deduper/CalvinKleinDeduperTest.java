package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class CalvinKleinDeduperTest extends DeduperTest {
	@Test
    public void testCalvinKleinDeduper() throws IOException, URISyntaxException {
    	final List<String> uniquesAttrs = getUniqueAttributes("CalvinKlein.js", "http://www.calvinklein.com/shop/en/ck/sale/womens-tanks-tees-sale/11556734");
    	assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("11556734", uniquesAttrs.get(0));
        
        final List<String> uniquesAttrs2 = getUniqueAttributes("CalvinKlein.js", "http://www.calvinklein.com/shop/en/ck/mens-clothing/mens-shirts/22707911");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("22707911", uniquesAttrs2.get(0));
        
        final List<String> uniquesAttrs3 = getUniqueAttributes("CalvinKlein.js", "http://www.calvinklein.com/shop/en/ck/sale/mens-shirts-final-sale/22869033-478");
        assertTrue(uniquesAttrs3 != null);
        assertEquals(1, uniquesAttrs3.size());
        assertEquals("22869033", uniquesAttrs3.get(0));
    }
}

package com.quadanalytix.merchantrules.deduper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.Test;

public class SmartPakEquineDeduperTest extends DeduperTest {
    @Test
    public void testSmartPakEquineDeduper() throws IOException, URISyntaxException {
        final List<String> uniquesAttrs = getUniqueAttributes("SmartPakEquine.js", "https://www.smartpakequine.com/pt/smartpak-trifecta-14-zip-top--12379");
        assertTrue(uniquesAttrs != null);
        assertEquals(1, uniquesAttrs.size());
        assertEquals("12379", uniquesAttrs.get(0));
        final List<String> uniquesAttrs1 = getUniqueAttributes("SmartPakEquine.js", "https://www.smartpakequine.com/pt/piper-breech-with-modified-rise-full-seat-by-smartpak-13087");
        assertTrue(uniquesAttrs1 != null);
        assertEquals(1, uniquesAttrs1.size());
        assertEquals("13087", uniquesAttrs1.get(0));
        final List<String> uniquesAttrs2 = getUniqueAttributes("SmartPakEquine.js", "https://www.smartpakequine.com/pt/piper-knee-patch-breeches-by-smartpak-10947");
        assertTrue(uniquesAttrs2 != null);
        assertEquals(1, uniquesAttrs2.size());
        assertEquals("10947", uniquesAttrs2.get(0));
    }
}
